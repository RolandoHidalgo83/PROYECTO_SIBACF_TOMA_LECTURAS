<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0">
    <title>Notificación de Revisión de Pago Reachazado - Orden {{ $ordenPago}} / COLAISA</title>
</head>
<body>
    <p>Estimad@: {{ $nombreSocio}}</p>
    </br>
    </br>
    <p>Se ha realizado la revisión de la orden de pago No. {{ $ordenPago}} con fecha {{ $fechaRevision}}</p>
    <p>por un monto de $ {{ $montoPago}} USD, a través del comprobante de pago No. {{ $numeroComprobante}}</p>
    <p>encontrandose novedades en la misma, por lo que no es aceptada.</p>
    <p>Favor comunicarse los la Junta de Recaudacion de Agua Potable para moyor detalle. Telenofo: 1234568</p>

    </br>
    </br>
    <ul>
    <p>Favor NO responder este correo, ya que es generado de manera automática.</p>
    <p>Gracias</p>
    </ul>
    </br>
    </br>
    <ul>
    <ul>
        <li>Powered by SIBACF</li>
        <li><a href="http://www.sibacf.com">Página principal de SICABF</a></li>
        <li>info@sibacf.com</li>
    </ul>
    </ul>

</body>
</html>