<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\General\Correo;

class NotificacionOrdenPago extends Mailable
{
    use Queueable, SerializesModels;
    public String $tipoNotificacion;
    public String $ordenPago;
    public String $numeroComprobante;
    public String $montoPago;
    public String $nombreSocio;
    public String $correoSocio;
    public String $fechaRevision;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $tipoNotificacion, String $ordenPago, String $numeroComprobante, String $montoPago, String $nombreSocio, String $correoSocio, String $fechaRevision)
    {
        $this->tipoNotificacion = $tipoNotificacion;
        $this->ordenPago = $ordenPago;
        $this->numeroComprobante = $numeroComprobante;
        $this->montoPago = $montoPago;
        $this->nombreSocio = $nombreSocio;
        $this->correoSocio = $correoSocio;
        $this->fechaRevision = $fechaRevision;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if ($this->tipoNotificacion == 'aprobada') {
        return $this->view('mails/notificacionAprobacionPago')->subject('Notificación de Revisión de Pago - Aprobación');
        }
        else {
           return $this->view('mails/notificacionRechazoPago')->subject('Notificación de Revisión de Pago - Rechazo');
        }
    }
}
