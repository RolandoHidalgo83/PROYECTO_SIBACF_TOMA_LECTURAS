<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
//use App\Models\General\Correo;


class Notificacion extends Mailable
{
    use Queueable, SerializesModels;
    public String $asunto;
    public String $mensaje;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $asunto, String $mensaje)
    {
        $this->asunto = $asunto;
        $this->mensaje = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.notificacion')->subject('ASUNTICO');
    }
}
