<?php

namespace app\Http\Controllers\API\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\Banco;

class BancoController extends Controller
{
    public function get_banco(Request $request)
    {
        $banco = new Banco; 
        $banco = $banco->get_banco();
        
        return response()->json($banco);
    }

    public function get_banco_id(Request $request, $id)
    {
        $banco = new Banco; 
        $banco = $banco->get_banco_id($id);
        
        return response()->json($banco);
    }

    public function delete_banco($id){
        $banco = new Banco; 
        $banco = $banco->delete_banco($id);        
        return response()->json($banco);

    }

    public function save_banco(Request $request)
    {
        $banco = new Banco; 
        $id = $request->input('id_banco');
        $nombre = $request->input('nombre');
        $nemonico = $request->input('nemonico');
        $fecha_creacion = $request->input('fecha_creacion');
        $usuario_creacion = $request->input('usuario_creacion');
        $fecha_actualizacion = $request->input('fecha_actualizacion');
        $usuario_actualizacion = $request->input('usuario_actualizacion');
        $estado_id = $request->input('estado_id');
        $objectSave = [
        //'id_banco' => $id,
        'nombre' => $nombre,
        'nemonico' => $nemonico,
        'fecha_creacion' => $fecha_creacion,
        'usuario_creacion' => $usuario_creacion,
        'fecha_actualizacion' => $fecha_actualizacion,
        'usuario_actualizacion' => $usuario_actualizacion,
        'estado_id'  => $estado_id,
        ];
        
        if($id != null){
            $data = $banco->update_banco($id, $objectSave);
        }else{
            $data = $banco->create_banco($objectSave);
        }
        return response()->json($data);
    }

}