<?php

namespace app\Http\Controllers\API\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\NotificacionOrdenPago;
use App\Mail\Notificacion;
use Illuminate\Support\Facades\Mail;
use App\Models\General\Correo;

class CorreoController extends Controller
{

    public function send_mail_id(Request $request, $tipoNotificacion, $ordenPago, $numeroComprobante, $montoPago, $nombreSocio, $correoSocio, $fechaRevision)
    {  //Numero de orden de pago
       //Numero de comprobante de pago
       //Monto de pago
       //Mensaje de aprobacion
       //Nombres del socio
       //Mail del socio 
       $correo = new Correo;
       $correo = [
        'tipoNotificacion' => $tipoNotificacion,  
        'ordenPago' => $ordenPago,
        'numeroComprobante' => $numeroComprobante,
        'montoPago' => $montoPago,
        'nombreSocio' => $nombreSocio,
        'correoSocio' => $correoSocio,
        'fechaRevision' => $fechaRevision,
    ];
        Mail::to($correoSocio)->send(new NotificacionOrdenPago($tipoNotificacion, $ordenPago, $numeroComprobante, $montoPago, $nombreSocio, $correoSocio, $fechaRevision));
    }

    public function enviar_notificacion(String $correoDestino, String $asunto, String $mensaje){
        Mail::to($correoDestino)->send(new Notificacion($asunto, $mensaje));
    }
}