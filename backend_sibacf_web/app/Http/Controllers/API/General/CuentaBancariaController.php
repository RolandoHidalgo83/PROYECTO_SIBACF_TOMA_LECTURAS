<?php

namespace app\Http\Controllers\API\General;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\General\CuentaBancaria;

class CuentaBancariaController extends Controller
{
    public function get_cuenta_bancaria(Request $request)
    {
        $cuentaBancaria = new CuentaBancaria; 
        $cuentaBancaria = $cuentaBancaria->get_cuenta_bancaria();        
        return response()->json($cuentaBancaria);
    }

    public function get_cuenta_bancaria_id(Request $request, $id)
    {
        $cuentaBancaria = new CuentaBancaria; 
        $cuentaBancaria = $cuentaBancaria->get_cuenta_bancaria_id($id);        
        return response()->json($cuentaBancaria);
    }

    public function get_cuentas_bancaria_nemonico(Request $request, $nemonico)
    {
        $cuentaBancaria = new CuentaBancaria;
        $cuentaBancaria = $cuentaBancaria->get_cuentas_bancaria_nemonico($nemonico);
        return response()->json($cuentaBancaria);
    }

    public function get_cuentas_por_banco_id(Request $request, $bancoId, $estadoNemonico)
    {
        $cuentaBancaria = new CuentaBancaria;
        $cuentaBancaria = $cuentaBancaria->get_cuentas_por_banco_id($bancoId, $estadoNemonico);
        return response()->json($cuentaBancaria);
    }

    public function save_cuenta_bancaria(Request $request)
    {
        $cuentaBancaria = new CuentaBancaria; 
        $id = $request->input('id_cuenta_bacaria');
        $banco_id = $request->input('banco_id');
        $tipo_cuenta_id = $request->input('tipo_cuenta_id');
        $cuenta = $request->input('cuenta');
        $nemonico = $request->input('nemonico');
        $fecha_creacion = $request->input('fecha_creacion');
        $usuario_creacion = $request->input('usuario_creacion');
        $fecha_actualizacion = $request->input('fecha_actualizacion');
        $usuario_actualizacion = $request->input('usuario_actualizacion');
        $estado_id = $request->input('estado_id');
 
        $objectSave = [
            'id_cuenta_bacaria' => $id_cuenta_bacaria,
            'banco_id' => $banco_id,
            'tipo_cuenta_id' => $tipo_cuenta_id,
            'cuenta' => $cuenta,
            'nemonico' => $nemonico,
            'fecha_creacion' => $fecha_creacion,
            'usuario_creacion' => $usuario_creacion,
            'fecha_actualizacion' => $fecha_actualizacion,
            'usuario_actualizacion' => $usuario_actualizacion,
            'estado_id'  => $estado_id,
        ];

        if($id != null){            
            $data = $cuentaBancaria->update_cuenta_bancaria($id, $objectSave);
        }else{
            $data = $cuentaBancaria->create_cuenta_bancaria($objectSave);
        }
        return response()->json($data);
    }

}