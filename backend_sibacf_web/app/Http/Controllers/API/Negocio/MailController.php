<?php

namespace App\Http\Controllers\API\Negocio;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Mail\CrearCorreo;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;


class MailController extends Controller
{
    //
    public function enviarCorreo()
    {
        $titulo = '[Confirmation] Thank you for your order';
        $detalleCliente = [ 
            'nombre' => 'Arogya'
            , 'direccion' => 'kathmandu Nepal'
            , 'telefono' => '123123123'
            , 'correo' => 'casa.rodrigo@hotmail.com' ];
            $order_details = [ 
                'SKU' => 'D-123456'
                , 'price' => '10000'
                , 'order_date' => '2020-01-22', ]; 
            $sendmail = Mail::to($detalleCliente['correo'])
            ->send(new CrearCorreo($titulo, $detalleCliente, $order_details)); 
            if (empty($sendmail)) 
            { 
                return response()->json(['message' => 'Correo Electrónico enviado exitosamente'], 200); 
            }
            else
            { 
                return response()->json(['message' => 'Envío de correo electrónico fallidol'], 400); 
            }
    }

    public function enviarCorreoConCredencialesAppWeb(Request $request, $destinatario, $usr, $clv)
    {
        log::info($request);
        log::info($destinatario);
        log::info($usr);
        log::info($clv);
        $titulo = '[Confirmation] Thank you for your order';
        $detalleCliente = [ 
            'nombre' => 'Arogya'
            , 'direccion' => 'kathmandu Nepal'
            , 'telefono' => '123123123'
            , 'correo' => 'casa.rodrigo@hotmail.com' ];
            $order_details = [ 
                'SKU' => 'D-123456'
                , 'price' => '10000'
                , 'order_date' => '2020-01-22', ]; 
            $sendmail = Mail::to($detalleCliente['correo'])
            ->send(new CrearCorreo($titulo, $detalleCliente, $order_details)); 
            if (empty($sendmail)) 
            { 
                return response()->json(['message' => 'Correo Electrónico enviado exitosamente'], 200); 
            }
            else
            { 
                return response()->json(['message' => 'Envío de correo electrónico fallidol'], 400); 
            }
    }
}
