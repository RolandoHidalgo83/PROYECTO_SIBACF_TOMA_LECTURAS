<?php

namespace app\Http\Controllers\API\Negocio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Negocio\DetallePago;
use Illuminate\Support\Facades\Log;

class DetallePagoController extends Controller
{
    public function get_detalle_pago(Request $request)
    {
        $detalle_pago = new DetallePago; 
        $detalle_pago = $detalle_pago->get_detalle_pago();        
        return response()->json($detalle_pago);
    }

    public function get_detalle_pago_id(Request $request, $id)
    {
        $detalle_pago = new DetallePago; 
        $detalle_pago = $detalle_pago->get_detalle_pago_id($id);        
        return response()->json($detalle_pago);
    }

    public function get_detalle_pago_por_transaccion_id(Request $request, $id)
    {
        $detalle_pago = new DetallePago; 
        $detalle_pago = $detalle_pago->get_detalle_pago_por_transaccion_id($id);        
        return response()->json($detalle_pago);
    }

    public function save_detalle_pago(Request $request)
    {
        $detalle = $request->all();
        //log::info($detalle);
        foreach($detalle as $registro)
            {
                $detalle_pago = new DetallePago; 
                $r = [];
                $r = $registro;
                $conceptoPagoId = 0;
                if(isset($r['id_detalle_pago'])){
                    $conceptoPagoId = $r['id_detalle_pago'];
                }
                if($conceptoPagoId == 0){
                    $detalle_pago->create_detalle_pago($r);
                }else{
                    $detalle_pago->update_detalle_pago($conceptoPagoId, $r);
                }
                
    }
        //return response()->json($data);
    }

}