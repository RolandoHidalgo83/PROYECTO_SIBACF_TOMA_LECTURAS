<?php

namespace app\Http\Controllers\API\Negocio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Negocio\Serie;

class SerieController extends Controller
{
    public function get_serie(Request $request)
    {
        $serie = new Serie; 
        $serie = $serie->get_serie();        
        return response()->json($serie);
    }

    public function get_serie_id(Request $request, $id)
    {
        $serie = new Serie; 
        $serie = $serie->get_serie_id($id);        
        return response()->json($serie);
    }

    public function save_serie(Request $request)
    {
        $registro = [];
        $registro = $request->all();
        $id = $request->input('id_serie');
        $serie = new Serie;

        if($id != null){            
            $data = $serie->update_serie($id, $registro);
        }else{
            $data = $serie->create_serie($registro);
        }
        return response()->json($data);
    }

}