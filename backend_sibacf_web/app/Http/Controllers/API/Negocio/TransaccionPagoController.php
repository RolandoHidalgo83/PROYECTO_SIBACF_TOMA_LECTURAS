<?php

namespace app\Http\Controllers\API\Negocio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Negocio\TransaccionPago;
use Illuminate\Support\Facades\Log;
use App\Models\Negocio\DetalleTransaccionPago;
use App\Models\General\Catalogo;
use App\Models\General\Persona;
use App\Http\Controllers\API\General\CorreoController;

class TransaccionPagoController extends Controller
{
    public function get_transaccion_pago(Request $request)
    {
        $transaccion_pago = new TransaccionPago; 
        $transaccion_pago = $transaccion_pago->get_transaccion_pago();        
        return response()->json($transaccion_pago);
    }

    public function get_transaccion_pago_id(Request $request, $id)
    {
        $transaccion_pago = new TransaccionPago; 
        $transaccion_pago = $transaccion_pago->get_transaccion_pago_id($id);        
        return response()->json($transaccion_pago);
    }

    public function get_transaccion_pago_por_persona_empresa_id_por_nemonico_estado($personaEmpresaId, $estados){
        $transaccion_pago = new TransaccionPago;
        //log::info($estados);
        $transaccion_pago = $transaccion_pago->get_transaccion_pago_por_persona_empresa_id_por_nemonico_estado($personaEmpresaId, $estados);
        return response()->json($transaccion_pago);
    }

    public function save_transaccion_pago(Request $request)
    {
        
        $transaccion_pago = new TransaccionPago; 
        $id = $request->input('id_transaccion_pago');
        $empresa_id = $request->input('empresa_id');
        $origen_pago_id = $request->input('origen_pago_id');
        $numero_documento = $request->input('numero_documento');
        $fecha_generacion_documento = $request->input('fecha_generacion_documento');
        $nombre_archivo = $request->input('nombre_archivo');
        $alias_archivo  = $request->input('alias_archivo');
        $path_archivo = $request->input('path_archivo');
        $observacion_pago = $request->input('observacion_pago');
        $fecha_creacion = $request->input('fecha_creacion');
        $usuario_creacion = $request->input('usuario_creacion');
        $fecha_actualizacion = $request->input('fecha_actualizacion');
        $usuario_actualizacion = $request->input('usuario_actualizacion');
        $estado_id = $request->input('estado_id');
        
 
        $objectSave = [
            'empresa_id' => $empresa_id,
            'origen_pago_id' => $origen_pago_id,
            'numero_documento' => $numero_documento,
            'fecha_generacion_documento' => $fecha_generacion_documento,
            'nombre_archivo' => $nombre_archivo,
            'alias_archivo' => $alias_archivo,
            'path_archivo' => $path_archivo,
            'observacion_pago' => $observacion_pago,
            'fecha_creacion' => $fecha_creacion,
            'usuario_creacion' => $usuario_creacion,
            'fecha_actualizacion' => $fecha_actualizacion,
            'usuario_actualizacion' => $usuario_actualizacion,
            'estado_id' => $estado_id,
        ];

        if($id != null){            
            $data = $transaccion_pago->update_transaccion_pago($id, $objectSave);
        }else{
            //log::info($objectSave);
            $data = $transaccion_pago->create_transaccion_pago($objectSave);
            $detalle = $request->all()['detalle'];    
            $catalogo = new Catalogo; 
            $catalogo = $catalogo->get_catalogo_nemonico_grupocatalogo_nemonico('CNSM', 'CNCPTPGO');
            $concepto =  (array) $catalogo;
            foreach($detalle as $registro)
            {
                //log::info($registro);
                //log::info('Dato Id: '.$data['id_transaccion_pago']);
                $detalleTransaccionPago = new DetalleTransaccionPago();
                $transaccion_pago_id = $data['id_transaccion_pago'];
                
                $concepto_pago_id = $concepto['id_catalogo'];
                if(isset($registro['concepto_pago_id'])){
                    $concepto_pago_id = $registro['concepto_pago_id'];
                }
                $consumo_id = $registro['consumo_id'];
                $valor_pago = $registro['valor_pago'];
                $fecha_creacion = $registro['fecha_creacion'];
                $usuario_creacion = $registro['usuario_creacion'];
                $fecha_actualizacion = $registro['fecha_actualizacion'];
                $usuario_actualizacion = $registro['usuario_actualizacion'];
                $estado_id = $registro['estado_id'];
                $objectDetSave = [
                    'transaccion_pago_id' => $transaccion_pago_id,
                    'concepto_pago_id' => $concepto_pago_id,
                    'consumo_id' => $consumo_id,
                    'valor_pago' => $valor_pago,
                    'fecha_creacion' => $fecha_creacion,
                    'usuario_creacion' => $usuario_creacion,
                    'fecha_actualizacion' => $fecha_actualizacion,
                    'usuario_actualizacion' => $usuario_actualizacion,
                    'estado_id' => $estado_id,
                ];
                $detalleTransaccionPago->create_detalle_transaccion_pago($objectDetSave);
                
            }
        }
        return response()->json($data); 
    }
    /**
     * Permite actualizar el estado de la transacción
     */
    public function update_transaccion_pago(Request $request){
        $registro = [];
        $registro = $request->all();
        $id = $request->input('id_transaccion_pago');
        $transaccion_pago = new TransaccionPago;
        $data = $transaccion_pago->update_transaccion_pago($id, $registro);
        $catalogo = new Catalogo;
        $estado = $catalogo->get_catalogo_id($data['estado_id']);
        if($estado['nemonico'] != 'REGI'){
            $persona = new Persona;
            $abonado = $persona->get_persona_por_transaccion_pago_id($data ['id_transaccion_pago']);
            if($abonado[0]->correo_electronico){   
                $mensaje = 'Estimad@ '.$abonado[0]->razon_social;
                $mensaje = $mensaje.'<br><br>Se ha realizado la revisión del pago No CP'.$data ['id_transaccion_pago'].'.';
                $mensaje = $mensaje.'<br>El pago se encuentra '.$estado['nombre'];
                if($estado['nemonico'] === 'RCH'){
                    $mensaje = $mensaje.' por la siguiente razón:';
                    $mensaje = $mensaje.'<br><br>'.$data ['observacion_pago'];
                    $mensaje = $mensaje.'<br><br>Realice la corrección de la información y vuelva a enviar para su revisión';
                }else{
                    $mensaje = $mensaje.'. Inmediatamente se procederá a emitir la factura física correspondiente; la misma reposará en nuestro archivo hasta que pueda acercarse a retirar de manera presencial';
                }
                try{
                    $correoController = new CorreoController();
                    $correoController->enviar_notificacion($abonado[0]->correo_electronico
                    , 'Revisión de pago J.A.A.P. Colaisa'
                    , $mensaje);
                }catch(Exception $e){

                }
            }
        }
        return response()->json($data);
    }
}