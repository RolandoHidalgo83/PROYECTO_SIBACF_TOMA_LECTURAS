<?php

namespace app\Http\Controllers\API\Negocio;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Negocio\AsignacionSerie;
use Illuminate\Support\Facades\Log;

class AsignacionSerieController extends Controller
{
    public function get_asignacion_serie(Request $request)
    {
        $asignacion_serie = new AsignacionSerie; 
        $asignacion_serie = $asignacion_serie->get_asignacion_serie();        
        return response()->json($asignacion_serie);
    }

    public function get_asignacion_serie_id(Request $request, $id)
    {
        $asignacion_serie = new AsignacionSerie; 
        $asignacion_serie = $asignacion_serie->get_asignacion_serie_id($id);        
        return response()->json($asignacion_serie);
    }

    public function get_asignacion_serie_persona_empresa(Request $reuqest, $identificacionPersona, $identificacionEmpresa, $estadoNemonico){
        $asignacion_serie = new AsignacionSerie;
        $asignacion_serie = $asignacion_serie->get_asignacion_serie_persona_empresa($identificacionPersona, $identificacionEmpresa, $estadoNemonico);
        return response()->json($asignacion_serie);
    }

    public function get_asignacion_activa_por_filtro($filtro){
        $asignacion_serie = new AsignacionSerie;
        $asignacion_serie = $asignacion_serie->get_asignacion_activa_por_filtro($filtro);
        return response()->json($asignacion_serie);
    }

    public function save_asignacion_serie(Request $request)
    {
        $registro = [];
        $registro = $request->all();
        $id = $request->input('id_asignacion_serie');
        $asignacion_serie = new AsignacionSerie;         
        if($id != null){
            $data = $asignacion_serie->update_asignacion_serie($id, $registro);
        }else{
            $data = $asignacion_serie->create_asignacion_serie($registro);
        }
        return response()->json($data);
    }

}