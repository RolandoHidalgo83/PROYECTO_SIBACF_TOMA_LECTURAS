<?php

namespace app\Http\Controllers\API\Seguridades;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seguridades\UsuarioRol;
use Illuminate\Support\Arr;

class UsuarioRolController extends Controller
{
    public function get_usuario_rol(Request $request)
    {
        $usuario_rol = new UsuarioRol; 
        $usuario_rol = $usuario_rol->get_usuario_rol();        
        return response()->json($usuario_rol);
    }

    public function get_usuario_rol_id(Request $request, $id)
    {
        $usuario_rol = new UsuarioRol; 
        $usuario_rol = $usuario_rol->get_usuario_rol_id($id);        
        return response()->json($usuario_rol);
    }

    public function get_usuario_rol_pro_usuario_id($id)
    {
        $usuario_rol = new UsuarioRol; 
        $usuario_rol = $usuario_rol->get_usuario_rol_pro_usuario_id($id);        
        return response()->json($usuario_rol);
    }

    public function save_usuario_rol(Request $request)
    {
        $registro = [];
        $registro = $request->all();
        $usuario_rol = new UsuarioRol; 
        $id = null;
        if (Arr::exists($registro, "id_usuario_rol")) {
            $id = $registro['id_usuario_rol'];
        }
        if($id != null){            
            $data = $usuario_rol->update_usuario_rol($id, $registro);
        }else{
            $data = $usuario_rol->create_usuario_rol($registro);
        }
        return response()->json($data);
    }

}