<?php

namespace app\Http\Controllers\API\Seguridades;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Seguridades\Usuario;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Arr;
use App\Http\Controllers\API\General\CorreoController;
use App\Models\Seguridades\UsuarioRol;
use App\Models\General\Persona;

class UsuarioController extends Controller
{
    public function get_usuario_por_tipo_usuario_id(Request $request, $tipoUsuarioId)
    {
        $usuario = new Usuario; 
        $usuario = $usuario->get_usuario_por_tipo_usuario_id($tipoUsuarioId);
        return response()->json($usuario);
    }

    public function get_usuario_id(Request $request, $id)
    {
        $usuario = new Usuario; 
        $usuario = $usuario->get_usuario_id($id);        
        return response()->json($usuario);
    }

    public function get_acceso_login_pass(Request $request, $login, $pass)
    {
        $usuario = new Usuario; 
        $usuario = $usuario->get_acceso_login_pass($login, $pass);
        return response()->json($usuario);
    }

    public function save_usuario(Request $request)
    {
        $registro = [];
        $registro = $request->all();
        //log::info($registro);
        $usuario = new Usuario;
        $id = null;
        $passAleatorio = substr(rand(), 0, 8);
        if (Arr::exists($registro, "id_usuario")) {
            $id = $registro['id_usuario'];
        }
        $rol_id = null;
        if (Arr::exists($registro, "rol_id")) {
            $rol_id = $registro['rol_id'];
            unset($registro['rol_id']);
        }
        if($id != null){
            $data = $usuario->update_usuario($id, $registro);
        }else{
            if($registro['tipo_usuario_id'] != 57){
                $registro['pass'] = md5($passAleatorio);
            }
            $data = $usuario->create_usuario($registro);
        }
        if($rol_id != null){
            $usuarioRol = new UsuarioRol; 
            $usuario_rol = $usuarioRol->get_usuario_rol_pro_usuario_id($data["id_usuario"]);
            if ($usuario_rol->isEmpty()) {
                $nuevo_usuario_rol = array(
                    "rol_id" => $rol_id,
                    "usuario_id" => $data['id_usuario'],
                    "fecha_creacion" => $data['fecha_creacion'],
                    "usuario_creacion" => $data['usuario_creacion'],
                    "fecha_actualizacion" => $data['fecha_actualizacion'],
                    "usuario_actualizacion" => $data['usuario_actualizacion'],
                    "estado_id" => $data['estado_id']
                );
                $usuarioRol->create_usuario_rol($nuevo_usuario_rol);
            }else{
                if($usuario_rol['rol_id'] != $rol_id){
                    $usuario_rol["rol_id"] = $rol_id;
                    $usuario_rol["fecha_actualizacion"] = $data['fecha_actualizacion'];
                    $usuario_rol["usuario_actualizacion"] = $data['usuario_actualizacion'];
                    $usuarioRol->update_usuario_rol($usuario_rol['rol_id'], $usuario_rol);
                }
            }
        }
        if($id === null && $registro['tipo_usuario_id'] != 57){
            $persona = new Persona;
            $prs = $persona->get_persona_por_identificacion($data['login']);
            //$prs = $persona->get_persona_por_id_persona_empresa($registro['persona_empresa_id']);
            if($prs["correo_electronico"]){    
                $correoController = new CorreoController();
                log::info($passAleatorio);
                try {
                    $correoController->enviar_notificacion($prs["correo_electronico"]
                    , 'Generación de usuario J.A.A.P. Colaisa'
                    , 'Estimad@ '.$prs["razon_social"]
                    .'<br><br>Se le comunica que se ha generado el usuario para uso en el'
                    .' Sistema WEB de la Junta Administradora de Agua Potable Colaisa.'
                    .' Cuando se autentique por primera vez en el sistema, le solicitará que cambie de clave.'
                    .'<br><br>Las credenciales de acceso son:'
                    .'<br><br>URL: <a href="http://www.jaapcolaisa.com">www.jaapcolaisa.com</a>'
                    .'<br>Usuraio: '.$registro['login']
                    .'<br>Contraseña: '.$passAleatorio
                    );
                }catch (Exception $e) { 
                }
            }
        }
        return response()->json($data); 
    }

    public function delete_usuario($id){
        $usuario = new Usuario; 
        $usuario = $usuario->delete_usuario($id);        
        return response()->json($usuario);

    }
}