<?php

namespace app\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Parametro extends Model
{
    protected $shema = 'sch_general';
    protected $table = 'sch_general.parametro';
    public $timestamps = false;
    protected $primaryKey = 'id_parametro';
    protected $fillable = [
        'id_parametro',
        'grupo_parametro_id',
        'empresa_id',
        'modulo_id',
        'nombre',
        'descripcion',
        'nemonico',
        'codigo_auxiliar',
        'valor_auxiliar',
        'valor',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_parametro()
    {
        $result = DB::table('sch_general.parametro')->get();
        return $result;
    }

    public function get_parametro_id($id)
    {
        $result = Parametro::where('id_parametro',$id)->first();
        return $result;
    }

    public function get_parametro_nemonico($nemonico)
    {
        $result = Parametro::where('nemonico',$nemonico)->first();
        return $result;
    }

    public function get_parametro_grupoparametro_nemonico($nemonico)
    {
        $result = DB::table('sch_general.parametro')
        ->join('sch_general.grupo_parametro','sch_general.parametro.grupo_parametro_id','=','sch_general.grupo_parametro.id_grupo_parametro')
        ->where('sch_general.grupo_parametro.nemonico',$nemonico)
        ->select('sch_general.parametro.*')
        ->get();
        return $result;
    }

    public function get_parametro_nemonico_grupoparametro_nemonico($nemonicoParametro, $nemonicoGrupoParametro)
    {
        $result = DB::table('sch_general.parametro')
        ->join('sch_general.grupo_parametro','sch_general.parametro.grupo_parametro_id','=','sch_general.grupo_parametro.id_grupo_parametro')
        ->where('sch_general.parametro.nemonico',$nemonicoParametro)
        ->where('sch_general.grupo_parametro.nemonico',$nemonicoGrupoParametro)
        ->select('sch_general.parametro.*')
        ->first();
        return $result;
    }

    public function create_parametro($objectSave)
    {
       $rowCreated = Parametro::create($objectSave);
       $response = Parametro::where('id_parametro',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_parametro($id, $objectSave)
    {
        $update = Parametro::where('id_catalogo',$id)->update($objectSave);
        $response = Parametro::where('id_parametro',$id)->first();
        return $response;
    }

    public function delete_parametro($id)
    {
        $response = Parametro::find($id)->delete();
        return $response;
    }
}