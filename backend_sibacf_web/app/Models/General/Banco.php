<?php

namespace app\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Banco extends Model
{
    
    protected $shema = 'sch_general';
    protected $table = 'sch_general.banco';
    public $timestamps = false;
    protected $primaryKey = 'id_banco';
    protected $fillable = [
        'id_banco',
        'nombre',
        'nemonico',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_banco()
    {
        $result = DB::table('sch_general.banco')->get();
        return $result;
    }

    public function get_banco_id($id)
    {
        $result = Banco::where('id_banco',$id)->first();
        return $result;
    }

    public function get_banco_nemonico($nemonico)
    {
        $result = Banco::where('nemonico',$nemonico)->first();
        return $result;
    }

    public function create_banco($objectSave)
    {  
        
       $rowCreated = Banco::create($objectSave);
       $response = Banco::where('id_banco',$rowCreated->id_banco)->first();
       return $response;//$rowCreated->id_banco;
    }

    public function update_banco($id, $objectSave)
    {
        $update =  Banco::where('id_banco',$id)->update($objectSave);
        $response = Banco::where('id_banco',$id)->first();
        return $response;
    }

    public function delete_banco($id)    {
         Banco::where('id_banco',$id)->delete();
    }
}