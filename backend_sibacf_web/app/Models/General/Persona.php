<?php

namespace app\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Persona extends Model
{
    protected $shema = 'sch_general';
    protected $table = 'sch_general.persona';
    public $timestamps = false;
    protected $primaryKey = 'id_persona';
    protected $fillable = [
        'id_persona',
        'tipo_identificacion_id',
        'identificacion',
        'personeria_id',
        'nombres',
        'apellidos',
        'razon_social',
        'nombre_comercial',
        'genero_id',
        'fecha_nacimiento',
        'estado_civil_id',
        'tipo_sangre_id',
        'trato_id',
        'codigo',
        'es_relacionado',
        'telefono_fijo',
        'extension_fijo',
        'telefono_alterno_fijo',
        'extension_alterno_fijo',
        'telefono_movil',
        'correo_electronico',
        'cc_correo_electronico',
        'pagina_web',
        'tipo_dicapacidad_id',
        'porcentaje_discapacidad',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_persona()
    {
        $result = DB::table('sch_general.persona')->get();
        return $result;
    }

    public function get_persona_id($id)
    {
        $result = Persona::where('id_persona',$id)->first();
        return $result;
    }

    public function get_persona_por_empresa_id_por_codigo_grupo_por_filtro($empresaId, $codigoGrupo, $filtro)
    {
        if($filtro == '@')
        {
            $filtro = '';
        }
        $result = DB::table('sch_general.persona as p')
        ->join('sch_general.persona_empresa as pe', 'p.id_persona', 'pe.persona_id')
        ->join('sch_general.tipo_persona as t', 'p.id_persona', 't.persona_id')
        ->join('sch_general.catalogo as c', 't.tipo_id', 'c.id_catalogo')
        ->where('pe.empresa_id', $empresaId)
        ->where('c.nemonico', $codigoGrupo)
        ->where(function ($qry) use ($filtro) {
            $qry->where('p.razon_social', 'like', '%'.$filtro.'%');
            $qry->orWhere('p.identificacion', 'like', '%'.$filtro.'%');
        }
        )
        ->select('p.*', 'pe.id_persona_empresa as persona_empresa_id')
        ->take(5)
        ->get()
        ;
        return $result;
    }

    public function create_persona($objectSave)
    {
       $rowCreated = Persona::create($objectSave);
       $response = Persona::where('id_persona',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_persona($id, $objectSave)
    {
        $update = Persona::where('id_catalogo',$id)->update($objectSave);
        $response = Persona::where('id_persona',$id)->first();
        return $response;
    }

    public function delete_persona($id)
    {
        $response = Persona::find($id)->delete();
        return $response;
    }

    public function get_persona_por_identificacion($identificacion)
    {
        $result = Persona::where('identificacion',$identificacion)->first();
        return $result;
    }

    public function get_persona_por_transaccion_pago_id($transaccionPagoId){
        $result = DB::table('sch_negocio.transaccion_pago as tp')
        ->join('sch_negocio.detalle_transaccion_pago as dp', 'tp.id_transaccion_pago', 'dp.transaccion_pago_id')
        ->join('sch_negocio.consumo as c', 'dp.consumo_id', 'c.id_consumo')
        ->join('sch_negocio.asignacion_serie as a', 'c.asignacion_serie_id', 'a.id_asignacion_serie')
        ->join('sch_general.persona_empresa as pe', 'a.persona_empresa_id', 'pe.id_persona_empresa')
        ->join('sch_general.persona as p', 'pe.persona_id', 'p.id_persona')
        ->where('tp.id_transaccion_pago', $transaccionPagoId)
        ->select('p.*')
        ->get();
        return $result;
    }

    public function get_persona_por_id_persona_empresa($idPersonaEmpresa)
    {
        $result = DB::table('sch_general.persona as p')
        ->join('sch_general.persona_empresa as pe', 'pe.persona_id', 'p.id_persona')
        ->where('pe.id_persona_empresa', $idPersonaEmpresa)
        ->select('p.*')
        ->get();
        return $result;
    }
}