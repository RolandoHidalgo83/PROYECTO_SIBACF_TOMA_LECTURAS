<?php

namespace app\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Periodo extends Model
{
    protected $shema = 'sch_general';
    protected $table = 'sch_general.empresa';
    public $timestamps = false;
    protected $primaryKey = 'id_periodo';
    protected $fillable = [
        'id_periodo',
        'empresa_id',
        'nombre',
        'anio',
        'mes',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_periodo()
    {
        $result = DB::table('sch_general.periodo')->get();
        return $result;
    }

    public function get_periodo_id($id)
    {
        $result = Periodo::where('id_periodo',$id)->first();
        return $result;
    }

    public function get_anios()
    {
        $result = DB::table('sch_general.periodo as periodo')
        ->select('periodo.anio')
        ->distinct()
        ->get(['periodo.anio']);
        return $result;
    }

    public function get_periodo_anio($anio, $nemonico)
    {
        $result = DB::table('sch_general.periodo')
        ->join('sch_general.catalogo','sch_general.periodo.estado_id','=','sch_general.catalogo.id_catalogo')
        ->where('anio',$anio)
        ->where('sch_general.catalogo.nemonico',$nemonico)
        ->select('sch_general.periodo.*')
        ->get();
        return $result;
    }

    public function create_periodo($objectSave)
    {
       $rowCreated = Periodo::create($objectSave);
       $response = Periodo::where('id_periodo',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_periodo($id, $objectSave)
    {
        $update = Periodo::where('id_catalogo',$id)->update($objectSave);
        $response = Periodo::where('id_periodo',$id)->first();
        return $response;
    }

    public function delete_periodo($id)
    {
        $response = Periodo::find($id)->delete();
        return $response;
    }
}