<?php

namespace app\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Catalogo extends Model
{
    protected $shema = 'sch_general';
    protected $table = 'sch_general.catalogo';
    public $timestamps = false;
    protected $primaryKey = 'id_catalogo';
    protected $fillable = [
        'id_catalogo',
        'grupo_catalogo_id',
        'nombre',
        'descripcion',
        'nemonico',
        'codigo_auxiliar',
        'valor_auxiliar',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_catalogo()
    {
        $result = DB::table('sch_general.catalogo')->get();
        return $result;
    }

    public function get_catalogo_id($id)
    {
        $result = Catalogo::where('id_catalogo',$id)->first();
        return $result;
    }

    public function get_catalogo_nemonico($nemonico)
    {
        $result = Catalogo::where('nemonico',$nemonico)->first();
        return $result;
    }

    public function get_catalogo_grupocatalogo_nemonico($nemonico)
    {
        $result = DB::table('sch_general.catalogo')
        ->join('sch_general.grupo_catalogo','sch_general.catalogo.grupo_catalogo_id','=','sch_general.grupo_catalogo.id_grupo_catalogo')
        ->where('sch_general.grupo_catalogo.nemonico',$nemonico)
        ->select('sch_general.catalogo.*')
        ->get();
        return $result;
    }

    public function get_catalogo_nemonico_grupocatalogo_nemonico($nemonicoCatalogo, $nemonicoGrupoCatalogo)
    {
        $result = DB::table('sch_general.catalogo')
        ->join('sch_general.grupo_catalogo','sch_general.catalogo.grupo_catalogo_id','=','sch_general.grupo_catalogo.id_grupo_catalogo')
        ->where('sch_general.catalogo.nemonico',$nemonicoCatalogo)
        ->where('sch_general.grupo_catalogo.nemonico',$nemonicoGrupoCatalogo)
        ->select('sch_general.catalogo.*')
        ->first();
        return $result;
    }

    public function create_catalogo($objectSave)
    {
       $rowCreated = Catalogo::create($objectSave);
       $response = Catalogo::where('id_catalogo',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_catalogo($id, $objectSave)
    {
        $update = Catalogo::where('id_catalogo',$id)->update($objectSave);
        $response = Catalogo::where('id_catalogo',$id)->first();
        return $response;
    }

    public function delete_catalogo($id)
    {
        $response = Catalogo::find($id)->delete();
        return $response;
    }
}