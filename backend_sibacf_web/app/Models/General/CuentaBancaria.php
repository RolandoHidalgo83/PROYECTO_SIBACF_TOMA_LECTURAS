<?php

namespace app\Models\General;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class CuentaBancaria extends Model
{
    protected $shema = 'sch_general';
    protected $table = 'sch_general.cuenta_bancaria';
    public $timestamps = false;
    protected $primaryKey = 'id_cuenta_bacaria';
    protected $fillable = [
        'id_cuenta_bacaria',
        'banco_id',
        'tipo_cuenta_id',
        'cuenta',
        'nemonico',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];

    public function get_cuentas_bancarias()
    {
        $result = DB::table('sch_general.cuenta_bancaria')->get();
        return $result;
    }

    public function get_cuentas_bancaria_id($id)
    {
        $result = CuentaBancaria::where('id_cuenta_bacaria',$id)->first();
        return $result;
    }

    public function get_cuentas_bancaria_nemonico($nemonico)
    {
        $result = CuentaBancaria::where('nemonico',$nemonico)->first();
        return $result;
    }

    public function get_cuentas_por_banco_id($bancoId, $estadoNemonico)
    {
        $result = DB::table('sch_general.cuenta_bancaria as cuenta_bancaria')
                    ->join('sch_general.banco as banco','cuenta_bancaria.banco_id','banco.id_banco')
                    ->join('sch_general.catalogo as catalogo', 'cuenta_bancaria.estado_id', 'catalogo.id_catalogo')
                    ->where('banco.id_banco',$bancoId)
                    ->where('catalogo.nemonico',$estadoNemonico)
                    ->select('cuenta_bancaria.*')
                    ->get();
        return $result;
    }

    public function create_banco($objectSave)
    {
       $rowCreated = CuentaBancaria::create($objectSave);
       $response = CuentaBancaria::where('id_cuenta_bancaria',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_banco($id, $objectSave)
    {
        $update = CuentaBancaria::where('id_catalogo',$id)->update($objectSave);
        $response = CuentaBancaria::where('id_cuenta_bancaria',$id)->first();
        return $response;
    }

    public function delete_cuenta_bancaria($id)
    {
        $response = CuentaBancaria::find($id)->delete();
        return $response;
    }
}