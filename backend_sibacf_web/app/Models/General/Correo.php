<?php

namespace app\Models\General;

use Illuminate\Support\Facades\Log;

class Correo 
{ 
    public string $ordenPago;
    public string $numeroComprobante;
    public string $montoPago;
    public string $nombreSocio;
    public string $correoSocio;

}