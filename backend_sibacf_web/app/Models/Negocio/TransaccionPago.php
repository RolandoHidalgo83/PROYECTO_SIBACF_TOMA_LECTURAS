<?php

namespace app\Models\Negocio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TransaccionPago extends Model
{
    protected $shema = 'sch_negocio';
    protected $table = 'sch_negocio.transaccion_pago';
    public $timestamps = false;
    protected $primaryKey = 'id_transaccion_pago';
    protected $fillable = [
        'id_transaccion_pago',
        'empresa_id',
        'origen_pago_id',
        'numero_documento',
        'fecha_generacion_documento',
        'nombre_archivo',
        'alias_archivo',
        'path_archivo',
        'observacion_pago',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_transaccion_pago()
    {
        $result = DB::table('sch_negocio.transaccion_pago')->get();
        return $result;
    }

    public function get_transaccion_pago_por_persona_empresa_id_por_nemonico_estado($personaEmpresaId, $nemonicoEstado)
    {
        $Sql = 'SELECT a.id_transaccion_pago, a.numero_documento'
        .' , a.fecha_generacion_documento, p.identificacion, p.razon_social'
        .' , ROUND(SUM(b.valor_pago), 2) valor_comprobante'
        .' , a.observacion_pago'
        .' , e.nombre estado'
        .' FROM sch_general.catalogo e INNER JOIN sch_negocio.transaccion_pago a'
        .' ON e.id_catalogo = a.estado_id'
        .' INNER JOIN sch_negocio.detalle_transaccion_pago b'
        .' ON a.id_transaccion_pago = b.transaccion_pago_id'
        .' LEFT JOIN ('
            .' sch_negocio.consumo c INNER JOIN sch_negocio.asignacion_serie am'
            .' ON c.asignacion_serie_id = am.id_asignacion_serie'
            .' INNER JOIN sch_general.persona_empresa pe ON am.persona_empresa_id = pe.id_persona_empresa'
            .' INNER JOIN sch_general.persona p ON pe.persona_id = p.id_persona'
            .' /*INNER JOIN sch_negocio.serie m ON am.serie_id = m.id_serie*/'
            .' ) ON c.id_consumo = b.consumo_id'
            .' WHERE e.nemonico IN ('.$nemonicoEstado.')';
            if($personaEmpresaId != '0'){
                $Sql = $Sql.' AND'
                .' EXISTS (SELECT \'\' FROM sch_negocio.detalle_transaccion_pago dp1'
                .' INNER JOIN sch_negocio.consumo c1 ON dp1.consumo_id = c1.id_consumo'
                .' INNER JOIN sch_negocio.asignacion_serie as1 ON c1.asignacion_serie_id = as1.id_asignacion_serie'
                .' INNER JOIN sch_general.persona_empresa pe1 ON as1.persona_empresa_id = pe1.id_persona_empresa'
                .' WHERE pe1.id_persona_empresa = '.$personaEmpresaId.' AND dp1.transaccion_pago_id = a.id_transaccion_pago)';
            }
            $Sql = $Sql.' GROUP BY a.id_transaccion_pago, a.numero_documento, a.fecha_generacion_documento'
            .' , p.identificacion, p.razon_social, a.observacion_pago, e.nombre';
            //$result = new Consumo;
            /* log::info($Sql); */
        $result = DB::select($Sql);
            
        return $result;
        /* $b= preg_split("/[,]/",$nemonicoEstado);
        $result = DB::table('sch_negocio.transaccion_pago')
        ->join('sch_general.catalogo', 'sch_negocio.transaccion_pago.estado_id', '=', 'sch_general.catalogo.id_catalogo')
        ->whereIn('sch_general.catalogo.nemonico', $b)//[$nemonicoEstado])
        ->select('sch_negocio.transaccion_pago.id_transaccion_pago'
        , 'sch_negocio.transaccion_pago.numero_documento'
        , 'sch_negocio.transaccion_pago.fecha_generacion_documento'
        , 
        , 'sch_negocio.transaccion_pago.observacion_pago'
        )
        ->get();
        return $result; */
    }

    public function get_transaccion_pago_id($id)
    {
        $result = TransaccionPago::where('id_transaccion_pago',$id)->first();
        return $result;
    }

    public function create_transaccion_pago($objectSave)
    {
       $rowCreated = TransaccionPago::create($objectSave);
       $response = TransaccionPago::where('id_transaccion_pago',$rowCreated->id_transaccion_pago)->first();
       return $response;//$rowCreated->id;
    }

    public function update_transaccion_pago($id, $objectSave)
    {
        $update = TransaccionPago::where('id_transaccion_pago',$id)->update($objectSave);
        $response = TransaccionPago::where('id_transaccion_pago',$id)->first();
        return $response;
    }

    public function delete_transaccion_pago($id)
    {
        $response = TransaccionPago::find($id)->delete();
        return $response;
    }
}