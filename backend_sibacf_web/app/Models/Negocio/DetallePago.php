<?php

namespace app\Models\Negocio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class DetallePago extends Model
{
    protected $shema = 'sch_negocio';
    protected $table = 'sch_negocio.detalle_pago';
    public $timestamps = false;
    protected $primaryKey = 'id_detalle_pago';
    protected $fillable = [
        'id_detalle_pago',
        'transaccion_pago_id',
        'forma_pago_id',
        'banco_id',
        'cuenta_bancaria',
        'cuenta_bancaria_id',
        'referencia',
        'valor_pago',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_detalle_pago()
    {
        $result = DB::table('sch_negocio.detalle_pago')->get();
        return $result;
    }

    public function get_detalle_pago_id($id)
    {
        $result = DetallePago::where('id_detalle_pago',$id)->first();
        return $result;
    }

    public function get_detalle_pago_por_transaccion_id($id)
    {
        //log::info($id);
        $nemonicoEstado = 'ACT';
        $result = DB::table('sch_negocio.detalle_pago as dp')
        ->join('sch_general.catalogo as e', 'dp.estado_id', 'e.id_catalogo')
        ->join('sch_general.catalogo as fp', 'dp.forma_pago_id', 'fp.id_catalogo')
        ->join('sch_general.cuenta_bancaria as cb', 'dp.cuenta_bancaria_id', 'cb.id_cuenta_bancaria')
        ->join('sch_general.banco as b', 'cb.banco_id', 'b.id_banco')
        ->leftjoin('sch_general.banco as bo', 'dp.banco_id', 'bo.id_banco')
        ->where('dp.transaccion_pago_id',$id)
        ->where('e.nemonico', $nemonicoEstado)
        ->select('dp.*', 'fp.nombre as forma_pago', 'bo.nombre as banco_origen'
        , 'b.nombre as banco_destino', 'cb.cuenta as cuenta_bancaria_destino'
        , 'e.nemonico'
        )
        ->get();
        return $result;
    }

    public function create_detalle_pago($objectSave)
    {
       $rowCreated = DetallePago::create($objectSave);
       $response = DetallePago::where('id_detalle_pago',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_detalle_pago($id, $objectSave)
    {
        $update = DetallePago::where('id_detalle_pago',$id)->update($objectSave);
        $response = DetallePago::where('id_detalle_pago',$id)->first();
        return $response;
    }

    public function delete_detalle_pago($id)
    {
        $response = DetallePago::find($id)->delete();
        return $response;
    }
}