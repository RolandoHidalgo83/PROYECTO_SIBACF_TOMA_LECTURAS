<?php

namespace app\Models\Negocio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class DetalleTransaccionPago extends Model
{
    protected $shema = 'sch_negocio';
    protected $table = 'sch_negocio.detalle_transaccion_pago';
    public $timestamps = false;
    protected $primaryKey = 'id_detalle_transaccion_pago';
    protected $fillable = [
        'id_detalle_transaccion_pago',
        'transaccion_pago_id',
        'concepto_pago_id',
        'consumo_id',
        'valor_pago',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_detalle_transaccion_pago()
    {
        $result = DB::table('sch_negocio.detalle_transaccion_pago')->get();
        return $result;
    }

    public function get_detalle_transaccion_pago_id($id)
    {
        $result = DetalleTransaccionPago::where('id_detalle_transaccion_pago',$id)->first();
        return $result;
    }

    public function get_detalle_transaccion_pago_por_transaccion_id($id)
    {
        $result = DB::table('sch_negocio.detalle_transaccion_pago as b')
        ->join('sch_general.catalogo as c', 'b.concepto_pago_id', 'c.id_catalogo')
        ->join('sch_negocio.consumo as l', 'b.consumo_id', '=', 'l.id_consumo')
        ->join('sch_general.periodo as p', 'l.periodo_id', '=', 'p.id_periodo')
        ->join('sch_negocio.asignacion_serie as am', 'l.asignacion_serie_id', '=', 'am.id_asignacion_serie')
        ->join('sch_general.catalogo as s', 'am.sector_id', '=', 's.id_catalogo')
        ->join('sch_negocio.serie as m', 'am.serie_id', '=', 'm.id_serie')
        ->where('b.transaccion_pago_id',$id)
        ->select('b.*'
        , 'c.nemonico as nemonico_concepto', 'c.nombre as nombre_concepto'
        , 's.nombre as barrio', 'm.codigo as medidor', 'p.anio as anio', 'p.nombre as mes'
        , DB::raw("l.valor_consumo_basico + l.valor_consumo_excedente + l.valor_comsumo_umbral as valor_consumo")
        , DB::raw("b.valor_pago - (l.valor_consumo_basico + l.valor_consumo_excedente + l.valor_comsumo_umbral) as valor_mora")
        )
        ->get();
        return $result;
    }

    public function create_detalle_transaccion_pago($objectSave)
    {
       $rowCreated = DetalleTransaccionPago::create($objectSave);
       $response = DetalleTransaccionPago::where('id_detalle_transaccion_pago',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_detalle_transaccion_pago($id, $objectSave)
    {
        $update = DetalleTransaccionPago::where('id_detalle_transaccion_pago',$id)->update($objectSave);
        $response = DetalleTransaccionPago::where('id_detalle_transaccion_pago',$id)->first();
        return $response;
    }

    public function delete_detalle_transaccion_pago($id)
    {
        $response = DetalleTransaccionPago::find($id)->delete();
        return $response;
    }
}