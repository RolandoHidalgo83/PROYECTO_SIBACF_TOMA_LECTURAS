<?php

namespace app\Models\Negocio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Consumo extends Model
{
    protected $shema = 'sch_negocio';
    protected $table = 'sch_negocio.consumo';
    public $timestamps = false;
    protected $primaryKey = 'id_consumo';
    protected $fillable = [
        'id_consumo',
        'asignacion_serie_id',
        'periodo_id',
        'fecha_toma_lectura',
        'lectura_anterior',
        'lectura_actual',
        'fecha_maxima_pago',
        'observacion',
        'consumo_basico_id',
        'excedente_id',
        'umbral_id',
        'valor_consumo_basico',
        'valor_consumo_excedente',
        'valor_consumo_umbral',
        'valor_multa_acumulada',
        'valor_total',
        'abono',
        'saldo',
        'fecha_ultima_consulta',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_consumo()
    {
        $result = DB::table('sch_negocio.consumo as consumo')->get();
        return $result;
    }

    public function get_consumo_id($id)
    {
        $result = Consumo::where('id_consumo',$id)->first();
        return $result;
    }

    public function get_consumo_socio($identificacionPersona, $identificacionEmpresa, $estado)
    {
        $result = DB::table('sch_negocio.consumo as consumo')
        ->join('sch_negocio.asignacion_serie as asignacion_serie','consumo.asignacion_serie_id','asignacion_serie.id_asignacion_serie')
        ->join('sch_general.catalogo as estado','consumo.estado_id','estado.id_catalogo')
        ->join('sch_general.catalogo as sector','asignacion_serie.sector_id','sector.id_catalogo')
        ->join('sch_negocio.serie as serie','asignacion_serie.serie_id','serie.id_serie')
        ->join('sch_general.persona_empresa as persona_empresa','asignacion_serie.persona_empresa_id','persona_empresa.id_persona_empresa')
        ->join('sch_general.persona as persona','persona_empresa.persona_id','persona.id_persona')
        ->join('sch_general.empresa as empresa','persona_empresa.empresa_id','empresa.id_empresa')
        ->where('estado.nemonico', $estado)
        ->where('persona.identificacion', $identificacionPersona)
        ->where('empresa.identificacion', $identificacionEmpresa)
        ->select('consumo.*','persona.identificacion','persona.apellidos','persona.nombres','persona.correo_electronico','sector.nombre','serie.codigo')
        ->get();
        return $result;
    }
   
    private function get_inicializar_consumo($id) {
        $Sql = 'INSERT INTO sch_negocio.consumo(asignacion_serie_id,periodo_id,fecha_toma_lectura,lectura_anterior,lectura_actual,fecha_maxima_pago,observacion,consumo_basico_id,'
        .'excedente_id,umbral_id,valor_consumo_basico,valor_consumo_excedente,valor_comsumo_umbral,valor_multa_acumulada,valor_total,abono,saldo,fecha_ultima_consulta,'
        .'fecha_creacion,usuario_creacion,fecha_actualizacion,usuario_actualizacion,estado_id) '
        .'SELECT a.id_asignacion_serie, '.$id.' periodo_id, now() fecha_toma_lectura '
        .', COALESCE((SELECT lectura_actual FROM sch_negocio.consumo c1 WHERE c1.periodo_id = '
        .'( '
        .'SELECT id_periodo FROM sch_general.periodo p2 INNER JOIN ( '
        .'SELECT EXTRACT(YEAR FROM p1.fecha_inicio + INTERVAL \'-1 day\') anio '
        .', EXTRACT(MONTH FROM p1.fecha_inicio + INTERVAL \'-1 day\') mes '
        .'FROM sch_general.periodo p1 WHERE p1.id_periodo = '.$id
        .') t1 ON p2.anio = t1.anio AND p2.mes = t1.mes '
        .') AND c1.asignacion_serie_id = a.id_asignacion_serie), 0) lectura_anterior '
        .', 0 lectura_actual '
        .', now() fecha_maxima_pago, \'\' observacion '
        .', (SELECT p.id_parametro FROM sch_general.grupo_parametro g INNER JOIN sch_general.parametro p'
        .'  ON g.id_grupo_parametro = p.grupo_parametro_id '
        .'  WHERE g.nemonico = \'REC\' AND p.nemonico = \'VALCONBASNOR\') consumo_basico_Id '
        .', (SELECT p.id_parametro FROM sch_general.grupo_parametro g INNER JOIN sch_general.parametro p '
        .'  ON g.id_grupo_parametro = p.grupo_parametro_id '
        .'  WHERE g.nemonico = \'REC\' AND p.nemonico = \'PUNOREXC\') excedente_id '
        .', (SELECT p.id_parametro FROM sch_general.grupo_parametro g INNER JOIN sch_general.parametro p '
        .'  ON g.id_grupo_parametro = p.grupo_parametro_id '
        .'  WHERE g.nemonico = \'REC\' AND p.nemonico = \'VALMULNOREXC\') umbral_id '
        .', 0 valor_consumo_basico, 0 valor_consumo_excedente, 0 valor_comsumo_umbral, 0 valor_multa_acumulada, 0 valor_total, 0 abono, 0 saldo '
        .', now() fecha_ultima_consulta, now() fecha_creacion, 1 usuario_creacion, now() fecha_actualizacion, 1 usuario_actualizacion '
        .', (SELECT c.id_catalogo FROM sch_general.grupo_catalogo g INNER JOIN sch_general.catalogo c '
        .'   ON g.id_grupo_catalogo = c.grupo_catalogo_id WHERE c.NEMONICO = \'CEGEN\') estado_id '
        .'FROM sch_negocio.asignacion_serie a INNER JOIN sch_general.catalogo ea ON a.estado_id = ea.id_catalogo '
        .'AND ea.nemonico = \'ACT\' '
        .'AND NOT EXISTS(SELECT \'\' MSG FROM sch_negocio.consumo c '
        .''
        .' WHERE c.asignacion_serie_id = a.id_asignacion_serie AND c.periodo_id = '.$id.');';
        //log::info($Sql);
        $result = DB::select($Sql);
    } 

    public function get_comsumo_inicializado($id, $nemonicoEstado, $personaEmpresaId,$consulta) {
        if($nemonicoEstado == 'CEGEN'){

            self::get_inicializar_consumo($id);
        }
        if($consulta == '@'){
            $consulta = '';
        }
        $Sql = 'SELECT c.id_consumo, p.identificacion, p.razon_social, i.nombre item '
        .', s.codigo, b.nombre barrio, m.anio, m.nombre mes, c.fecha_toma_lectura '
        .', CASE WHEN e.NEMONICO IN(\'CEGEN\', \'REGI\') THEN'

        .' CASE WHEN c.lectura_anterior = 0 THEN '
        .' COALESCE((SELECT lectura_actual FROM sch_negocio.consumo c1 WHERE c1.periodo_id = '
        .'( '
        .'SELECT id_periodo FROM sch_general.periodo p2 INNER JOIN ( '
        .'SELECT EXTRACT(YEAR FROM p1.fecha_inicio + INTERVAL \'-1 day\') anio '
        .', EXTRACT(MONTH FROM p1.fecha_inicio + INTERVAL \'-1 day\') mes '
        .'FROM sch_general.periodo p1 WHERE p1.id_periodo = '.$id
        .') t1 ON p2.anio = t1.anio AND p2.mes = t1.mes '
        .') AND c1.asignacion_serie_id = a.id_asignacion_serie), 0) '
        .'ELSE c.lectura_anterior END '
        .'ELSE c.lectura_anterior END lectura_anterior '
        .', c.lectura_actual'
        .', c.fecha_maxima_pago'
        .', CASE WHEN c.lectura_actual < c.lectura_anterior THEN 0 ELSE c.lectura_actual - c.lectura_anterior END consumo '
        .', ROUND(c.valor_consumo_basico, 2) valor_consumo_basico'
        .', ROUND(c.valor_consumo_excedente, 2) valor_excedente'
        .', ROUND(c.valor_comsumo_umbral, 2) valor_adicional'
        .', ROUND(c.valor_consumo_basico + c.valor_consumo_excedente + c.valor_comsumo_umbral, 2) sub_total'
        .', ROUND(c.valor_multa_acumulada, 2) valor_mora'
        .', ROUND(c.valor_total, 2) total'
        .', ROUND(c.saldo, 2) saldo'
        .' FROM '
        .'sch_negocio.item i '
        .'INNER JOIN sch_negocio.serie s ON i.id_item = s.articulo_id '
        .'INNER JOIN sch_negocio.asignacion_serie a ON s.id_serie = a.serie_id '
        .'INNER JOIN sch_general.catalogo b ON a.sector_id = b.id_catalogo '
        .'INNER JOIN sch_general.persona_empresa pe ON a.persona_empresa_id = pe.id_persona_empresa '
        .'INNER JOIN sch_general.persona p ON pe.persona_id = p.id_persona '
        .'INNER JOIN sch_negocio.consumo c ON a.id_asignacion_serie = c.asignacion_serie_id '
        .'INNER JOIN sch_general.catalogo e ON c.estado_id = e.id_catalogo '
        .'INNER JOIN sch_general.periodo m ON c.periodo_id = m.id_periodo '        
        .'WHERE '
        .'e.NEMONICO = \''.$nemonicoEstado.'\'';
        if($id != 0){
            $Sql = $Sql.' AND periodo_id = '.$id;
        }
        if($personaEmpresaId != 0){
            $Sql = $Sql.' AND a.persona_empresa_id = '.$personaEmpresaId;
            $Sql = $Sql.' AND 1 = ';
            $Sql = $Sql.' COALESCE((SELECT DISTINCT 2 FROM sch_negocio.transaccion_pago a1'
            .' INNER JOIN sch_negocio.detalle_transaccion_pago b1'
            .' ON a1.id_transaccion_pago = b1.transaccion_pago_id'
            .' INNER JOIN sch_general.catalogo c1 ON a1.estado_id = c1.id_catalogo'
            .' AND c1.nemonico<>\'ANU\''
            .' WHERE b1.consumo_id = c.id_consumo), 1)';
        }
        $Sql = $Sql.' AND (p.identificacion like \'%'.$consulta.'%\' OR p.razon_social like \'%'.$consulta.'%\' OR s.codigo like \'%'.$consulta.'%\')'
        .' ORDER BY b.nombre, p.razon_social'
        ;
        $result = new Consumo;
        $result = DB::select($Sql);
            //log::info($Sql);
        return $result;
    }

    public function get_aprobar_lectura_consumo($Ids)
    {
        $Sql = ' UPDATE sch_negocio.consumo c SET'
        .' valor_consumo_basico = t2.valorbasico'
        .' , valor_consumo_excedente = t2.valorexceso'
        .' , valor_comsumo_umbral = t2.valorumbral'
        .' , valor_total = t2.total'
        .' , saldo = t2.total'
        .' , fecha_actualizacion = NOW()'
        .' , estado_id = t2.estado_id'
        .' , fecha_maxima_pago = NOW()'
        .' FROM'
        .' ('
            .' SELECT t1.*'
        .' , t1.consumoexceso * t1.puexceso valorexceso'
        .' , t1.consumoumbral * t1.puumbral valorumbral'
        .' , t1.valorbasico + (t1.consumoexceso * t1.puexceso) + (t1.consumoumbral * t1.puumbral) total'
        .' , (SELECT id_catalogo FROM sch_general.grupo_catalogo g INNER JOIN sch_general.catalogo gc'
        .' ON g.id_grupo_catalogo = gc.grupo_catalogo_id'
        .' WHERE g.nemonico = \'EST\' AND gc.nemonico = \'APRV\') estado_id'
        .' FROM ('
            .' SELECT t.id_consumo, t.consumo, t.limitebasico consumobasico'
        .' , CASE WHEN t.consumo <= t.limitebasico THEN 0 ELSE t.consumo - t.limitebasico END excesogeneral'
        .' , CASE WHEN'
        .' CASE WHEN t.consumo <= t.limitebasico THEN 0 ELSE t.consumo - t.limitebasico END'
        .' > t.limiteexceso THEN t.limiteexceso ELSE'
        .' CASE WHEN t.consumo <= t.limitebasico THEN 0 ELSE t.consumo - t.limitebasico END'
        .' END consumoexceso'
        .' , CASE WHEN'
        .' CASE WHEN t.consumo <= t.limitebasico THEN 0 ELSE t.consumo - t.limitebasico END'
        .' - t.limiteexceso > 0 THEN'
        .' CASE WHEN t.consumo <= t.limitebasico THEN 0 ELSE t.consumo - t.limitebasico END'
        .' - t.limiteexceso'
        .' ELSE 0 END'
        .' consumoumbral'
        .' , t.valorbasico, t.puExceso, t.puumbral'
        .' FROM ('
            .' SELECT c.id_consumo, CASE WHEN c.lectura_actual < c.lectura_anterior THEN 0'
            .' ELSE c.lectura_actual - c.lectura_anterior END'
            .' CONSUMO'
            .' , (SELECT CAST(valor AS numeric(28,12)) FROM sch_general.parametro WHERE NEMONICO = \'LIMBASNOR\') limitebasico'
            .' , (SELECT CAST(valor AS numeric(28,12)) FROM sch_general.parametro WHERE NEMONICO = \'LIMNOREXCCON\') limiteexceso'
            .' , (SELECT CAST(valor AS numeric(28,12)) FROM sch_general.parametro WHERE NEMONICO = \'VALCONBASNOR\') valorbasico'
            .' , (SELECT CAST(valor AS numeric(28,12)) FROM sch_general.parametro WHERE NEMONICO = \'PUNOREXC\') puExceso'
            .' , (SELECT CAST(valor AS numeric(28,12)) FROM sch_general.parametro WHERE NEMONICO = \'VALMULNOREXC\') puumbral'
            .' FROM sch_negocio.consumo c'
            .' INNER JOIN sch_general.catalogo e ON c.estado_id = e.id_catalogo'
            .' WHERE e.nemonico = \'REGI\''
            .' AND c.id_consumo IN('.$Ids.')'
        .' ) t'
        .' ) t1'
        .' ) t2 WHERE c.id_consumo = t2.id_consumo';
        DB::select($Sql);
    }

    public function create_consumo($objectSave)
    {
       $rowCreated = Consumo::create($objectSave);
       $response = Consumo::where('id_consumo',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_consumo($id, $objectSave)
    {
        $update = Consumo::where('id_consumo',$id)->update($objectSave);
        $response = Consumo::where('id_consumo',$id)->first();
        return $response;
    }

    public function delete_consumo($id)
    {
        $response = Consumo::find($id)->delete();
        return $response;
    }
}