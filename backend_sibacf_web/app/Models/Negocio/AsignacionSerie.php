<?php

namespace app\Models\Negocio;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class AsignacionSerie extends Model
{
    protected $shema = 'sch_negocio';
    protected $table = 'sch_negocio.asignacion_serie';
    public $timestamps = false;
    protected $primaryKey = 'id_asignacion_serie';
    protected $fillable = [
        'id_asignacion_serie',
        'serie_id',
        'persona_empresa_id',
        'fecha_instalacion',
        'lectura_inicial',
        'ciclo',
        'sector_id',
        'ruta',
        'manzana',
        'numero_piso',
        'numero_casa',
        'numero_lote',
        'longitud',
        'latitud',
        'orden',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_asignacion_serie()
    {
        $result = DB::table('sch_negocio.asignacion_serie')->get();
        return $result;
    }

    public function get_asignacion_serie_id($id)
    {
        $result = AsignacionSerie::where('id_asignacion_serie',$id)->first();
        return $result;
    }

    public function get_asignacion_serie_persona_empresa ($identificaionPersona, $identificaionEmpresa, $estadoNemonico) {
        $result = DB::table('sch_negocio.asignacion_serie as asignacion_serie')
                    ->join('sch_general.catalogo as estado','asignacion_serie.estado_id','estado.id_catalogo')
                    ->join('sch_general.persona_empresa as persona_empresa', 'asignacion_serie.persona_empresa_id', 'persona_empresa.id_persona_empresa')
                    ->join('sch_general.persona as persona', 'persona_empresa.persona_id', 'persona.id_persona')
                    ->join('sch_general.empresa as empresa', 'persona_empresa.empresa_id', 'empresa.id_empresa')
                    ->where('estado.nemonico',$estadoNemonico)
                    ->where('persona.identificacion',$identificaionPersona)
                    ->where('empresa.identificacion',$identificaionEmpresa)
                    ->select('asignacion_serie.*')
                    ->get();
        return $result;
    }

    public function get_asignacion_activa_por_filtro($filtro){
        if($filtro == '@')
        {
            $filtro = '';
        }
        $result = DB::table('sch_negocio.asignacion_serie as a')
        ->join('sch_general.catalogo as e','a.estado_id','e.id_catalogo')
        ->join('sch_negocio.serie as s','a.serie_id','s.id_serie')
        ->join('sch_general.catalogo as b','a.sector_id','b.id_catalogo')
        ->join('sch_general.persona_empresa as pe', 'a.persona_empresa_id', 'pe.id_persona_empresa')
        ->join('sch_general.persona as p', 'pe.persona_id', 'p.id_persona')
        ->where('e.nemonico','ACT')
        ->where(function ($qry) use ($filtro){
            $qry->where('p.razon_social', 'like', '%'.$filtro.'%');
            $qry->orWhere('p.identificacion', 'like', '%'.$filtro.'%');
            $qry->orWhere('b.nombre', 'like', '%'.$filtro.'%');
            $qry->orWhere('s.codigo', 'like', '%'.$filtro.'%');
        }
        )
        ->select('a.id_asignacion_serie', 's.id_serie as serie_id'
        , 'p.identificacion', 'p.razon_social', 's.codigo as codigo_medidor'
        , 's.serie', 'b.nombre as barrio', 'a.orden')
        ->get();
        return $result;
    }

    public function create_asignacion_serie($objectSave)
    {
       $rowCreated = AsignacionSerie::create($objectSave);
       $response = AsignacionSerie::where('id_asignacion_serie',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_asignacion_serie($id, $objectSave)
    {
        $update = AsignacionSerie::where('id_asignacion_serie',$id)->update($objectSave);
        $response = AsignacionSerie::where('id_asignacion_serie',$id)->first();
        return $response;
    }

    public function delete_asignacion_serie($id)
    {
        $response = AsignacionSerie::find($id)->delete();
        return $response;
    }
}