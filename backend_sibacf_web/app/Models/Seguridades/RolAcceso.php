<?php

namespace app\Models\Seguridades;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class RolAcceso extends Model
{
    protected $shema = 'sch_seguridades';
    protected $table = 'sch_seguridades.rol_acceso';
    public $timestamps = false;
    protected $primaryKey = 'id_rol_acceso';
    protected $fillable = [
        'id_rol_acceso',
        'rol_id',
        'acceso_id',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id'
    ];


    public function get_rol_acceso()
    {
        $result = DB::table('sch_seguridades.rol_acceso')->get();
        return $result;
    }

    public function get_rol_acceso_id($id)
    {
        $result = RolAcceso::where('id_rol_acceso',$id)->first();
        return $result;
    }

    public function get_Accesos_Por_Usuario_Id($id)
    {
        $result = DB::select(
            ' SELECT'
        . ' DISTINCT a.id_acceso, a.acceso_padre_id, a.nombre texto'
        . ' , a.accion, a.nivel, a.orden, a.icono'
        . ' FROM sch_general.catalogo era'
        . ' INNER JOIN sch_seguridades.rol_acceso ra ON era.id_catalogo = ra.estado_id'
        . ' AND era.nemonico = \'ACT\''
        . ' INNER JOIN sch_seguridades.acceso a ON ra.acceso_id = a.id_acceso'
        . ' INNER JOIN sch_general.catalogo ea ON a.estado_id = ea.id_catalogo'
        . ' AND ea.nemonico = \'ACT\''
        . ' WHERE ra.rol_id IN(SELECT ur.rol_id'
        . ' FROM sch_seguridades.usuario_rol ur'
        . ' INNER JOIN sch_general.catalogo eu ON ur.estado_id = eu.id_catalogo'
        . ' AND eu.nemonico = \'ACT\''
        . ' WHERE ur.usuario_id = '.$id.')'
        . ' ORDER BY a.ORDEN, a.NIVEL'
    );
        return $result;
    }

    public function create_rol_acceso($objectSave)
    {
       $rowCreated = RolAcceso::create($objectSave);
       $response = RolAcceso::where('id_rol_acceso',$rowCreated->id)->first();
       return $response;//$rowCreated->id;
    }

    public function update_rol_acceso($id, $objectSave)
    {
        $update = RolAcceso::where('id_rol_acceso',$id)->update($objectSave);
        $response = RolAcceso::where('id_rol_acceso',$id)->first();
        return $response;
    }

    public function delete_rol_acceso($id)
    {
        $response = RolAcceso::find($id)->delete();
        return $response;
    }
}