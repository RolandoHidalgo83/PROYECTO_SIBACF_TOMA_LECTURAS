<?php

namespace app\Models\Seguridades;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Usuario extends Model
{
    protected $shema = 'sch_seguridades';
    protected $table = 'sch_seguridades.usuario';
    public $timestamps = false;
    protected $primaryKey = 'id_usuario';
    protected $fillable = [
        'id_usuario',
        'persona_empresa_id',
        'login',
        'pass',
        'confirmacion',
        'fecha_expiracion',
        'fecha_ultimo_acceso',
        'sesion_activa',
        'tipo_usuario_id',
        'fecha_creacion',
        'usuario_creacion',
        'fecha_actualizacion',
        'usuario_actualizacion',
        'estado_id',
    ];


    public function get_usuario_por_tipo_usuario_id($tipoUsuarioId)
    {
        $result = DB::table('sch_seguridades.usuario as u')
        ->join('sch_general.persona_empresa as pe', 'u.persona_empresa_id', 'pe.id_persona_empresa')
        ->join('sch_general.persona as p', 'pe.persona_id', 'p.id_persona')
        ->join('sch_general.catalogo as tu', 'u.tipo_usuario_id', 'tu.id_catalogo')
        ->join('sch_general.catalogo as e', 'u.estado_id', 'e.id_catalogo')
        ->leftjoin('sch_seguridades.usuario_rol as ur', 'u.id_usuario', 'ur.usuario_id')
        ->leftjoin('sch_seguridades.rol as r', 'ur.rol_id', 'r.id_rol')
        ->where('u.tipo_usuario_id', $tipoUsuarioId)
        ->select('u.*', 'tu.nombre as tipo_usuario', 'p.identificacion as identificacion'
        , 'p.razon_social as nombres', 'p.correo_electronico as correo_electronico'
        , 'e.nemonico as nemonico_estado', 'r.id_rol as perfil_id', 'r.nombre as perfil')
        ->get();
        return $result;
    }

    public function get_usuario_id($id)
    {
        $result = Usuario::where('id_usuario',$id)->first();
        return $result;
    }

    public function get_acceso_login_pass($login, $pass)
    {
        $result =  DB::table('sch_seguridades.usuario')
        ->leftjoin('sch_general.catalogo as tu', 'sch_seguridades.usuario.tipo_usuario_id', 'tu.id_catalogo')
        ->leftjoin('sch_general.persona_empresa', 'sch_seguridades.usuario.persona_empresa_id', '=', 'sch_general.persona_empresa.id_persona_empresa')
        ->leftjoin('sch_general.empresa', 'sch_general.persona_empresa.empresa_id', '=', 'sch_general.empresa.id_empresa')
        ->leftjoin('sch_general.persona', 'sch_general.persona_empresa.persona_id', '=', 'sch_general.persona.id_persona')
            ->where('sch_seguridades.usuario.login',$login)
            ->where('sch_seguridades.usuario.pass',$pass)
            ->select('sch_seguridades.usuario.id_usuario'
            , 'sch_seguridades.usuario.login as usuario'
            , 'sch_seguridades.usuario.tipo_usuario_id'
            , 'tu.nemonico as nemonico_tipo_usuario'
            , 'sch_general.persona_empresa.id_persona_empresa'
            , 'sch_general.persona.id_persona'
            , 'sch_general.persona.razon_social as persona'
            , 'sch_general.empresa.id_empresa'
            , 'sch_general.empresa.razon_social as empresa'
            , 'sch_seguridades.usuario.confirmacion'
            )
            ->first();

        return $result;
        
    }

    public function create_usuario($objectSave)
    {
       $rowCreated = Usuario::create($objectSave);
       //log::info($objectSave);
       $response = Usuario::where('id_usuario',$rowCreated->id_usuario)->first();
       return $response;//$rowCreated->id;
    }

    public function update_usuario($id, $objectSave)
    {
        $update = Usuario::where('id_usuario',$id)->update($objectSave);
        $response = Usuario::where('id_usuario',$id)->first();
        return $response;
    }

    public function delete_usuario($id)
    {
        $response = Usuario::find($id)->delete();
        return $response;
    }
}