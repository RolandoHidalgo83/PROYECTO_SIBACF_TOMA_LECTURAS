import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { observable, Observable } from 'rxjs';
import { Rol } from './rol';
import { UtilitarioService} from '../../servicio/utilitario.service';
import { SesionService } from '../../servicio/sesion.service';

@Injectable({
  providedIn: 'root'
})
export class RolService {
  endPointId: string = '/rol/getRolId';
  endPointNemonoico: string = '/rol/getRolNemonico';
  endPointListarTodoPorEmpresaId: string = '/rol/getRolPorEmpresaId';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService,
    private sesionService: SesionService
  ) { }

  obtenerRolId(id : string): Observable<Rol>
  {
    /* console.log(id);
    console.log(this.endPointId); */
    return this.http.get<Rol>(`${this.utilitarioService.getURL()}${this.endPointId}/${id}`);
  }

  obtenerRolNemonico(nemonico : string): Observable<Rol>
  {
    console.log(nemonico);
    console.log(this.endPointNemonoico);
    return this.http.get<Rol>(`${this.utilitarioService.getURL()}${this.endPointNemonoico}/${nemonico}`);
  }

  listarTodoPorEmpresaId(): Observable<Rol[]>
  {
    return this.http.get<Rol[]>(`${this.utilitarioService.getURL()}${this.endPointListarTodoPorEmpresaId}/${this.sesionService.getEmpresaId()}`);
  }
}

