export class Rol {
    
    id_rol: number;
    empresa_id: number;
    tipo_rol_id: number;
    nombre: string;
    descripcion: string;
    nemonico: string;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: number;

}
