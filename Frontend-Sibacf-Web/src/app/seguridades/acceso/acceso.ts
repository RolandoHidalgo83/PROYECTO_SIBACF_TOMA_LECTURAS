export class Acceso {

    id_acceso: bigint;
    nombre: string;
    descripcion: string;
    nemonico: string;
    acceso_padre_id: bigint;
    nivel: number;
    orden: number;
    accion: string;
    fecha_ultima_consulta: string;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;
    icono: string;

}
