export class UsuarioRol {
    id_usuario_rol: number;
    rol_id: number;
    usuario_id: number;
    fecha_creacion: string;
    usuario_creacion: number;
    fecha_actualizacion: string;
    usuario_actualizacion: number;
    estado_id: number;
}
