import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import { UsuarioRol } from './usuario-rol';

@Injectable({
  providedIn: 'root'
})
export class UsuarioRolService {
  private endPointListaPorUsuarioId: string = '/usuarioRol/getUsuarioRolPorUsuarioId';
  private endPointGrabar: string = '/usuarioRol/saveUsuarioRol';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService,

  ) { }
  /**
   * Permite consumir el sw que lista los registros por medio del id del usuario
   */
  listarPorUsuarioId(usuarioId: string): Observable<UsuarioRol[]> {
    return this.http.get<UsuarioRol[]>(`${this.utilitarioService.getURL()}${this.endPointListaPorUsuarioId}/${usuarioId}`);
  }

  guardar(objeto: Object): Observable<any> {
    return this.http.post<any>(`${this.utilitarioService.getURL()}${this.endPointGrabar}`, objeto);
  }
}
