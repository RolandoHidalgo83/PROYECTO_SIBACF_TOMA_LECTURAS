export class RolAcceso {

    id_rol_acceso:bigint ;
    rol_id: bigint;
    acceso_id: bigint;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;
}
