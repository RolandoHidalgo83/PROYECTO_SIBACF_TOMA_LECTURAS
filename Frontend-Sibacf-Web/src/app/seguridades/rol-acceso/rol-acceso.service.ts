import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Menu } from '../../modelo/menu';
import { UtilitarioService} from '../../servicio/utilitario.service';
import { SesionService} from '../../servicio/sesion.service';

@Injectable({
  providedIn: 'root'
})
export class RolAccesoService {
  private endPoint: string = '/rolAcceso/getAccesoPorUsuarioId';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService,
    private sesionService: SesionService
  ) { }
  /**
   * Obtiene una lista de opciones de menú que estan activos y corresponden a los roles asignados al usuario autenticado
   */
  listarAccesosActivosPorUsuarioAutenticado(): Observable<Menu[]>{
    return this.http.get<Menu[]>(`${this.utilitarioService.getURL()}${this.endPoint}/${this.sesionService.getUsuarioId()}`);
  };
}
