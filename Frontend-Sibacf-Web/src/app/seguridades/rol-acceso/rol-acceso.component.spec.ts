import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RolAccesoComponent } from './rol-acceso.component';

describe('RolAccesoComponent', () => {
  let component: RolAccesoComponent;
  let fixture: ComponentFixture<RolAccesoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RolAccesoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RolAccesoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
