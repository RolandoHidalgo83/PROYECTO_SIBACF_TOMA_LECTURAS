import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import { Usuario } from './usuario';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  private endPointListaPorTipoUsuarioId: string = '/usuario/getUsuarioPorTipoUsuarioId';
  private endPointObtenerPorUsuarioId: string = '/usuario/getUsuarioId';
  private endPointGrabar: string = '/usuario/saveUsuario';
  private endPointEliminar: string = '/usuario/deleteUsuario';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * Permite consumir el sw que lista todos los registros
   */
  listarPorTipoUsuarioId(tipoUsuarioId: string): Observable<Usuario[]> {
    return this.http.get<Usuario[]>(`${this.utilitarioService.getURL()}${this.endPointListaPorTipoUsuarioId}/${tipoUsuarioId}`);
  }
  /**
   * Permite obtener por medio del id del usuario
   * @param usuarioId 
   */
  obtenerPorUsuarioId(usuarioId: string): Observable<Usuario>{
    return this.http.get<Usuario>(`${this.utilitarioService.getURL()}${this.endPointObtenerPorUsuarioId}/${usuarioId}`);
  }
  /**
   * Permite consumir el sw que guarda la información
   * @param objeto
   */
  guardar(objeto: Object): Observable<Usuario> {
    return this.http.post<Usuario>(`${this.utilitarioService.getURL()}${this.endPointGrabar}`, objeto);
  }
  /**
   * Permite eliminar un registro por medio del id
   * @param id
   */
  eliminar(id: number): Observable<any>{
    //console.log(`${this.utilitarioService.getURL()}${this.endPointEliminar}/${id}`);
    return this.http.get<any>(`${this.utilitarioService.getURL()}${this.endPointEliminar}/${id}`);
  }
}
