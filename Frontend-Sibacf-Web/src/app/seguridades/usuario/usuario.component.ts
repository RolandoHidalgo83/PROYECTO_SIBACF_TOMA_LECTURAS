import { Component, OnInit, ViewChild } from '@angular/core';
import { SesionService } from '../../servicio/sesion.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Catalogo } from '../../administracion/catalogo/catalogo';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { DatePipe } from '@angular/common';
import { Usuario } from './usuario';
import { CatalogoService } from '../../administracion/catalogo/catalogo.service';
import { Rol } from '../rol/rol';
import { RolService } from '../rol/rol.service';
import { UsuarioService } from './usuario.service';
import { Socio } from '../../administracion/socio/socio';
import { SocioService } from '../../administracion/socio/socio.service';
import { UsuarioRolService } from '../usuario-rol/usuario-rol.service';
import { ControlContainer } from '@angular/forms';


@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css'],
  providers: [DatePipe]
})
export class UsuarioComponent implements OnInit {
  @ViewChild('closebutton') closebutton;
  @ViewChild('btnCerrarModalPerfil') closebuttonPerfil;
  ubicacion: string = 'Gestión de Usuarios';
  modoEdicion: boolean;
  tipoUsuarioSel: number;
  listaTipoUsuario: Catalogo[];
  listaUsuario: Usuario[];
  usuario: Usuario = new Usuario();
  listaSocio: Socio[];
  filtroSocio: string;
  rol: Rol = new Rol();
  listaPerfil: Rol[];
  campoUsuarioSoloLectura: boolean;
  confirmacionContrasena: string;
  activo: boolean;
  constructor(
    private sesionService: SesionService,
    private router: Router,
    private utilitarioService: UtilitarioService,
    private datePipe: DatePipe,
    private usuarioService: UsuarioService,
    private catalogoService: CatalogoService,
    private rolService: RolService,
    private socioService: SocioService,
    private usuarioRolService: UsuarioRolService
  ) { }

  ngOnInit(): void {
    if (this.sesionService.getUsuarioId()) {
      this.modoEdicion = false;
      this.listarTiposUsuario();
      this.listarPerfil();
    } else {
      this.router.navigate(['./login']);
    }
  }
  listarTiposUsuario() {
    this.catalogoService.obtenerPorNemonicoGrupo('TIPUSR')
      .subscribe(resp => {
        this.listaTipoUsuario = resp;
      });
  }
  /**
   * Permite ejecutar el evento change el drop down list
   */
  cambioTipoUsuario() {
    this.usuario = new Usuario();
    this.usuario.tipo_usuario_id = Number(this.tipoUsuarioSel);
    this.confirmacionContrasena = '';
    this.usuario.confirmacion = false;
    this.usuario.sesion_activa = false;
    this.activo = true;
    this.rol = new Rol();
    if (this.listaTipoUsuario.filter(a => a.id_catalogo == this.tipoUsuarioSel)[0].nemonico == 'TIPUSRINT') {
      this.campoUsuarioSoloLectura = false;
    } else {
      this.campoUsuarioSoloLectura = true;
      this.rol = this.listaPerfil.filter(a => a.nemonico == 'CLI')[0];
    }
    this.usuarioService.listarPorTipoUsuarioId(this.tipoUsuarioSel.toString())
      .subscribe(resp => {
        this.listaUsuario = resp;
        //console.log(this.listaUsuario);
      });
  }
  /**
   * Permite preparar los campos para un nuevo registro
   */
  nuevo() {
    this.modoEdicion = true;
    this.usuario = new Usuario();
    this.tipoUsuarioSel = undefined;
    this.usuario.confirmacion = false;
    this.usuario.sesion_activa = false;
    this.activo = true;
    this.confirmacionContrasena = '';
  }
  /**
   * Permite grabar la información
   */
  grabar() {
    let continuar = true;
    if (!this.tipoUsuarioSel) {
      continuar = false;
      Swal.fire({
        icon: 'error',
        text: "Especifique el tipo de usuario",
        showConfirmButton: false,
        timer: 1500
      });
    } else {
      if (this.listaTipoUsuario.filter(a => a.id_catalogo == Number(this.tipoUsuarioSel))[0].nemonico
        == 'TIPUSRINT') {
        if (this.usuario.pass == undefined) {
          continuar = false;
          Swal.fire({
            icon: 'error',
            text: "Especifique una contraseña",
            showConfirmButton: false,
            timer: 1500
          });
        } else {
          if (this.usuario.pass.trim() == '') {
            continuar = false;
            Swal.fire({
              icon: 'error',
              text: "Especifique una contraseña",
              showConfirmButton: false,
              timer: 1500
            });
          } else {
            if (this.usuario.pass != this.confirmacionContrasena) {
              continuar = false;
              Swal.fire({
                icon: 'error',
                text: "La contraseña y la confirmación son diferentes",
                showConfirmButton: false,
                timer: 1500
              });
            }
          }
        }
      } else {
        this.confirmacionContrasena = "prueba";//Math.random().toString(36).slice(-8);
        this.usuario.pass = this.confirmacionContrasena;
      }
    }
    if (continuar) {
      if (!this.usuario.login
        || !this.rol.id_rol
        || !this.usuario.persona_empresa_id
      ) {
        Swal.fire({
          icon: 'error',
          text: "Especifique la información requerida",
          showConfirmButton: false,
          timer: 1500
        });
      } else {
        this.usuario.pass = this.utilitarioService.getDatoEncriptadoEnMD5(this.usuario.pass);
        let fechaActual = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
        let usuarioId = Number(this.sesionService.getUsuarioId());
        let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
        let itm = lstEstados.filter(itm => itm.nemonico == 'ACT')[0];
        if (!this.activo) {
          itm = lstEstados.filter(itm => itm.nemonico == 'PAS')[0];
        }
        let usr;
        if (this.usuario.id_usuario == undefined) {
          usr = {
            persona_empresa_id: this.usuario.persona_empresa_id,
            login: this.usuario.login,
            pass: this.usuario.pass,
            confirmacion: this.usuario.confirmacion,
            sesion_activa: this.usuario.sesion_activa,
            tipo_usuario_id: this.usuario.tipo_usuario_id,
            fecha_creacion: fechaActual,
            usuario_creacion: usuarioId,
            fecha_actualizacion: fechaActual,
            usuario_actualizacion: usuarioId,
            estado_id: itm.id_catalogo,
            rol_id: this.rol.id_rol
          };
        } else {
          usr = {
            id_usuario: this.usuario.id_usuario,
            persona_empresa_id: this.usuario.persona_empresa_id,
            login: this.usuario.login,
            pass: this.usuario.pass,
            confirmacion: this.usuario.confirmacion,
            sesion_activa: this.usuario.sesion_activa,
            tipo_usuario_id: this.usuario.tipo_usuario_id,
            fecha_creacion: fechaActual,
            usuario_creacion: usuarioId,
            fecha_actualizacion: fechaActual,
            usuario_actualizacion: usuarioId,
            estado_id: itm.id_catalogo,
            rol_id: this.rol.id_rol
          };
        }
        this.usuarioService.guardar(usr)
          .subscribe(resp => {
            this.modoEdicion = false;
            this.usuarioService.listarPorTipoUsuarioId(this.tipoUsuarioSel.toString())
              .subscribe(resp => {
                this.listaUsuario = resp;
              });
            /*this.usuarioRolService.listarPorUsuarioId(resp.id_usuario.toString())
              .subscribe(resp1 => {
                let reg;
                if (resp1.length == 0) {
                  reg = {
                    rol_id: this.rol.id_rol,
                    usuario_id: resp.id_usuario,
                    fecha_creacion: fechaActual,
                    usuario_creacion: usuarioId,
                    fecha_actualizacion: fechaActual,
                    usuario_actualizacion: usuarioId,
                    estado_id: itm.id_catalogo
                  };
                } else {
                  reg = {
                    id_usuario_rol: resp1[0].id_usuario_rol,
                    rol_id: this.rol.id_rol,
                    fecha_actualizacion: fechaActual,
                    usuario_actualizacion: usuarioId,
                    estado_id: itm.id_catalogo
                  };
                }
                this.usuarioRolService.guardar(reg)
                  .subscribe(resp2 => {
                    
                  });
              });*/

          });
      }
    }
  }
  /**
   * Permite cancelar la edición de la información
   */
  cancelar() {
    this.modoEdicion = false;
  }
  /**
   * Permite cargar la información para mofificar
   * @param r 
   */
  editar(r: Usuario) {

    this.usuario = new Usuario();
    this.confirmacionContrasena = '';
    this.usuario = r;
    this.tipoUsuarioSel = this.usuario.tipo_usuario_id;
    this.usuario.pass = '';
    this.modoEdicion = true;
    this.rol = new Rol();
    //console.log(this.usuario.perfil_id);
    if (this.usuario.perfil_id != null) {
      this.rol = this.listaPerfil.filter(a => a.id_rol == this.usuario.perfil_id)[0];
    }
  }
  /**
   * Permite eliminar el registro previa confirmación
   * @param r 
   */
  eliminar(r: Usuario) {
    Swal.fire({
      text: "¿Esta seguro de que desea eliminar?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.usuario = new Usuario();
        this.usuario = r;
        this.usuarioService.eliminar(this.usuario.id_usuario)
          .subscribe(resp => {
            this.usuarioService.listarPorTipoUsuarioId(this.tipoUsuarioSel.toString())
              .subscribe(resp => {
                this.listaUsuario = resp;
              });
          });
      }
    });
  }
  seleccionSocio(r: Socio) {
    this.usuario.persona_empresa_id = Number(r.persona_empresa_id);
    this.usuario.identificacion = r.identificacion;
    this.usuario.nombres = r.razon_social;
    this.usuario.correo_electronico = r.correo_electronico;
    if (this.listaTipoUsuario.filter(a => a.id_catalogo == Number(this.tipoUsuarioSel))[0].nemonico == 'TIPUSREXT') {
      this.usuario.login = r.identificacion.trim();
    }
    this.closebutton.nativeElement.click();
    //console.log(this.usuario);
  }
  aplicarFiltroSocio() {
    if (this.tipoUsuarioSel == undefined) {
      this.closebutton.nativeElement.click();
      Swal.fire({
        icon: 'warning',
        text: "Seleccione un tipo de usuario",
        showConfirmButton: false,
        timer: 1500
      }
      );

    } else {
      this.socioService.listarPorFiltro(
        this.listaTipoUsuario.filter(a => a.id_catalogo == Number(this.tipoUsuarioSel))[0].nemonico
          == 'TIPUSRINT' ? 'TIPTRCEMP' : 'TIPTRCCLI'
        , this.filtroSocio)
        .subscribe(resp => {
          this.listaSocio = resp;
        });
    }
  }
  /**
   * Permite listar todos los perfiles
   */
  listarPerfil() {
    this.rolService.listarTodoPorEmpresaId()
      .subscribe(resp => {
        this.listaPerfil = resp;
        //console.log(this.listaPerfil);
      });
  }
  seleccionPerfil(perfil: Rol) {
    this.rol = new Rol();
    this.rol = perfil;
    this.closebuttonPerfil.nativeElement.click();
  }
}
