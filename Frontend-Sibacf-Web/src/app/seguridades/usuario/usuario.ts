export class Usuario {

    id_usuario: number;
    persona_empresa_id: number;
    login: string;
    pass: string;
    confirmacion: boolean;
    fecha_expiracion: string;
    fecha_ultimo_acceso: string;
    sesion_activa: boolean;
    fecha_creacion: string;
    usuario_creacion: number;
    fecha_actualizacion: string;
    usuario_actualizacion: number;
    estado_id: number;
    nemonico_estado: string;
    tipo_usuario_id: number;
    tipo_usuario: string;
    perfil_id: number;
    perfil: string;
    identificacion: string;
    nombres: string;
    correo_electronico: string;
}
