import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { UsuarioService } from '../usuario/usuario.service';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { DatePipe } from '@angular/common';
import { Usuario } from '../usuario/usuario';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-cambio-clave',
  templateUrl: './cambio-clave.component.html',
  styleUrls: ['./cambio-clave.component.css'],
  providers: [DatePipe]
})
export class CambioClaveComponent implements OnInit {
  claveActual: string;
  nuevaClave: string;
  confirmacionClave: string;
  usuarioId: string;
  usuario: Usuario;
  constructor(
    private router: Router,
    private rutaActiva: ActivatedRoute,
    private usuarioService: UsuarioService,
    private utilitarioService: UtilitarioService,
    private datePipe: DatePipe,
  ) { }

  ngOnInit(): void {
    this.usuarioId = this.rutaActiva.snapshot.paramMap.get('usuarioId')
    this.usuarioService.obtenerPorUsuarioId(this.usuarioId)
      .subscribe(resp => {
        this.usuario = resp;
      });
  }
  /**
   * Permite guardar la nueva clave
   */
  grabar() {
    if (this.nuevaClave == undefined) {
      Swal.fire({
        icon: 'error',
        title: 'Debe especificar la nueva clave',
        showConfirmButton: false,
        timer: 1500
      });
    } else {
      if (this.nuevaClave != this.confirmacionClave) {
        Swal.fire({
          icon: 'error',
          title: 'La nueva clave y la confirmación no coinciden',
          showConfirmButton: false,
          timer: 1500
        });
      } else {
        if (this.usuario.pass == this.utilitarioService.getDatoEncriptadoEnMD5(this.claveActual)) {
          let usuario = {
            id_usuario: this.usuarioId
            , pass: this.utilitarioService.getDatoEncriptadoEnMD5(this.nuevaClave)
            , confirmacion: true
            , fecha_actualizacion: this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss')
          };
          this.usuarioService.guardar(usuario)
            .subscribe(resp => {
              this.login();
            });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Contraseña actual incorrecta',
            showConfirmButton: false,
            timer: 1500
          });
        }
      }
    }
  }
  login() {
    this.router.navigate(['./login']);
  }
}
