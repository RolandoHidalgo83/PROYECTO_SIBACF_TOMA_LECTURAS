import { Component, OnInit, ViewChild } from '@angular/core';
import { SesionService } from '../../servicio/sesion.service';
import { RegistroConsumoService } from '../registro-consumo/registro-consumo.service';
import { RegistroConsumo } from '../registro-consumo/registro-consumo';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Banco } from '../../administracion/banco/banco';
import { BancoService } from '../../administracion/banco/banco.service';
import { CuentaBancaria } from '../../administracion/cuenta-bancaria/cuenta-bancaria';
import { CuentaBancariaService } from '../../administracion/cuenta-bancaria/cuenta-bancaria.service';
import { Catalogo } from '../../administracion/catalogo/catalogo';
import { CatalogoService } from '../../administracion/catalogo/catalogo.service';
import { DetallePago } from '../detalle-pago/detalle-pago';
import { ParametroService } from '../../administracion/parametro/parametro.service';
import { Parametro } from '../../administracion/parametro/parametro';
import { TransaccionPago } from '../transaccion-pago/transaccion-pago';
import { DatePipe } from '@angular/common';
import { TransaccionPagoService } from '../transaccion-pago/transaccion-pago.service';
import { DetalleTransaccionService } from '../detalle-transaccion/detalle-transaccion.service';
import { DetallePagoService } from '../detalle-pago/detalle-pago.service';
import { DetalleTransaccion } from '../detalle-transaccion/detalle-transaccion';
import { SocioService } from '../../administracion/socio/socio.service';
import { Socio } from '../../administracion/socio/socio';

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;


@Component({
  selector: 'app-pago',
  templateUrl: './pago.component.html',
  styleUrls: ['./pago.component.css'],
  providers: [DatePipe]
})
export class PagoComponent implements OnInit {
  @ViewChild('closebutton') closebutton;
  ubicacion: string = 'Aplicación de pagos';
  listaConsumo: RegistroConsumo[];
  filtro: string;
  personaEmpresaId: string;
  bancoOrigenSeleccionado: string;
  tipoPagoSeleccionado: string;
  listaBancos: Banco[];
  listaCuentaBanco: CuentaBancaria[];
  listaTipoPago: Catalogo[] = [];
  listaDetallePago: DetallePago[] = [];
  detallePago: DetallePago;
  bancoDestinoSeleccionado: number;
  //cuentaBancoDestinoSeleccionado: number;
  indexTab: number = 0;
  tipoRecargo: string = '';
  porcentajeRecargo: string = '';
  valorRecargo: string = '';
  periodicidadMora: string = '';
  listaSeleccionados: RegistroConsumo[] = [];
  totalConsumoComprobantePago: number = 0;
  totalMoraComprobantePago: number = 0;
  totalComprobantePago: number = 0;
  listaTransaccionesPago: TransaccionPago[];
  transaccion: TransaccionPago;
  valorComprobanteSeleccionado: number;
  cuentaOrigenVisible: boolean = false;
  listaSocios: Socio[];
  socio: Socio = new Socio();
  filtroSocio: string;
  nemonicoTipoUsuario: string;
  constructor(
    private sesionService: SesionService,
    private router: Router,
    private registroConsumoService: RegistroConsumoService,
    private bancoService: BancoService,
    private cuentaBancariaService: CuentaBancariaService,
    private tipoPagoServicio: CatalogoService,
    private catalogoService: CatalogoService,
    private parametroService: ParametroService,
    private datePipe: DatePipe,
    private transaccionPagoService: TransaccionPagoService,
    private detalleTransaccionService: DetalleTransaccionService,
    private detallePagoService: DetallePagoService,
    private socioService: SocioService
  ) { }

  ngOnInit(): void {
    if (this.sesionService.getUsuarioId()) {
      this.nemonicoTipoUsuario = this.sesionService.getNemonicoTipoUsuario();
      if (this.nemonicoTipoUsuario == 'TIPUSREXT') {
        this.personaEmpresaId = this.sesionService.getPersonaEmpresaId();
      }
      this.listarConsumosParaPago();
      this.listarTransaccionesPago();
      this.detallePago = new DetallePago();
      this.listarBancos();
      this.listarTiposPago();
      this.listarParametrosPorNemonicoGrupo();
      this.transaccion = new TransaccionPago();
    } else {
      this.router.navigate(['./login']);
    }
  }
  /**
   * Permite listar los parámetros por medio del nemónico del grupo
   */
  listarParametrosPorNemonicoGrupo() {
    this.parametroService.listarPorNemonicoGrupo('REC')
      .subscribe(resp => {
        let lst: Parametro[] = resp;
        this.tipoRecargo = lst.filter(a => a.nemonico == 'TIPREC')[0].valor;
        this.porcentajeRecargo = lst.filter(a => a.nemonico == 'TICRECPOR')[0].valor;
        this.valorRecargo = lst.filter(a => a.nemonico == 'TIPRECVAL')[0].valor;
        this.periodicidadMora = lst.filter(a => a.nemonico == 'PERMOR')[0].valor;

      });
  }
  /**
   * Permite listar los consumos con estado aprobado que pertenezcan a un id de persona empresa
   */
  listarConsumosParaPago() {
    if (this.personaEmpresaId != undefined) {
      this.registroConsumoService.listarConsumoInicializado('0', 'APRV', this.personaEmpresaId, this.filtro)
        .subscribe(resp => {
          this.listaConsumo = resp;
          this.listaConsumo.forEach(function (itm) { itm.seleccionado = false });
        });
    }
  }
  /**
   * Permite listar todos los registros de bancos
   */
  listarBancos() {
    this.bancoService.obtenerTodo()
      .subscribe(resp => {
        this.listaBancos = resp;
      });
  }
  listarTiposPago() {
    this.catalogoService.obtenerPorNemonicoGrupo('FP')
      .subscribe(resp => {
        this.listaTipoPago = resp;
      });
  }
  /**
   * Permite sumar los consumos seleccionados por el usuario
   */
  sumarSeleccionados(registro: RegistroConsumo) {
    registro.seleccionado = !registro.seleccionado;
    this.totalConsumoComprobantePago = 0;
    this.totalMoraComprobantePago = 0;
    this.totalComprobantePago = 0;
    registro.valor_mora = 0;
    registro.total = registro.sub_total;
    if (registro.seleccionado) {
      let tipoRecargo = this.tipoRecargo;
      let porcentajeRecargo = this.porcentajeRecargo;
      let valorRecargo = this.valorRecargo;
      var fechaMaximaPago: any = new Date(registro.fecha_maxima_pago);
      var fechaActual: any = new Date();
      var diasRetraso: any = Math.floor((fechaActual - fechaMaximaPago) / (1000 * 60 * 60 * 24));
      if (diasRetraso > 0) {
        let total = registro.total;
        if (tipoRecargo == '1') {
          registro.valor_mora = total * Number(porcentajeRecargo) / 100;
        } else {
          registro.valor_mora = Number(valorRecargo);
        }
        registro.total = Number(registro.sub_total) + Number(registro.valor_mora);
      }
      this.listaSeleccionados.push(registro);

      //this.listaSeleccionados = this.listaConsumo.filter(x => x.seleccionado);
      /* let lstSelecionados = this.listaConsumo.filter(x => x.seleccionado);
      this.totalPagosSeleccionados = lstSelecionados.reduce((a, b) =>
        a + b.saldo, 0 
      );*/
    } else {
      this.listaSeleccionados.splice(this.listaSeleccionados.indexOf(registro), 1);
    }
    this.totalConsumoComprobantePago = this.listaSeleccionados.reduce((a, b) => a + Number(b.sub_total), 0);
    this.totalMoraComprobantePago = this.listaSeleccionados.reduce((a, b) => a + Number(b.valor_mora), 0);
    this.totalComprobantePago = this.listaSeleccionados.reduce((a, b) => a + Number(b.total), 0);
  }
  /**
   * Permite obtener la lista de bancos disponibles
   */
  cambioBanco() {
    this.cuentaBancariaService.obtenerPorBancoId(
      this.bancoDestinoSeleccionado.toString(), 'ACT')
      .subscribe(resp => {
        this.listaCuentaBanco = resp;
      });

  }
  /**
   * Permite agregar el registro de pago
   */
  agregarRegistroPago() {
    if (this.transaccion.id_transaccion_pago == undefined) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'Seleccione un comprobante de pago',
        showConfirmButton: false,
        timer: 1500
      })
    } else {
      if (
        this.detallePago.forma_pago_id == undefined
        || this.detallePago.valor_pago == undefined
        || this.detallePago.cuenta_bancaria_id == undefined
        || this.detallePago.referencia == undefined
      ) {
        Swal.fire({
          position: 'top-end',
          icon: 'error',
          title: 'Especifique los datos requeridos',
          showConfirmButton: false,
          timer: 1500
        });
      } else {
        let continuar: boolean = true;
        if (this.listaTipoPago.filter(a => a.id_catalogo == this.detallePago.forma_pago_id)[0].nemonico
          != 'DEP') {
          if (this.detallePago.banco_id == undefined
            || this.detallePago.cuenta_bancaria_origen == undefined
          ) {
            Swal.fire({
              position: 'top-end',
              icon: 'error',
              title: 'Especifique los datos requeridos',
              showConfirmButton: false,
              timer: 1500
            });
          }
        }
        if (continuar) {
          this.detallePago.valor_pago = Number(this.detallePago.valor_pago.toFixed(2));
          this.detallePago.transaccion_pago_id = this.transaccion.id_transaccion_pago;
          if (this.detallePago.banco_id != undefined) {
            this.detallePago.banco_origen = this.listaBancos.filter(a => a.id_banco == this.detallePago.banco_id)[0].nombre;
          }
          this.detallePago.forma_pago = this.listaTipoPago.filter(a => a.id_catalogo == this.detallePago.forma_pago_id)[0].nombre;
          this.detallePago.banco_destino = this.listaBancos.filter
            (
              a => a.id_banco == (this.listaCuentaBanco
                .filter(b => b.id_cuenta_bancaria == this.detallePago.cuenta_bancaria_id)[0].banco_id)
            )[0].nombre;
          this.detallePago.cuenta_bancaria_destino = this.listaCuentaBanco.filter(b => b.id_cuenta_bancaria == this.detallePago.cuenta_bancaria_id)[0].cuenta;
          this.detallePago.id_detalle_pago = 0;
          this.listaDetallePago.push(this.detallePago);
          this.detallePago = new DetallePago();
          this.listaCuentaBanco = undefined;
          this.bancoDestinoSeleccionado = undefined;

        }
      }
    }
  }
  /**
   * Permite registrar la transacción de pago con estado GENERADO
   */
  generarComprobante() {
    if (this.listaSeleccionados.length == 0) {
      Swal.fire('', 'No ha seleccionado nungúna deuda', 'error');
    } else {
      let transaccionPago: TransaccionPago = new TransaccionPago();
      let fechaActual = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
      let usuarioId = Number(this.sesionService.getUsuarioId());
      let empresaId = this.sesionService.getEmpresaId();
      if (empresaId != '') {
        transaccionPago.empresa_id = Number(empresaId);
      }
      transaccionPago.origen_pago_id = Number(this.sesionService.getTipoUsuarioId());
      transaccionPago.numero_documento = '.';
      transaccionPago.fecha_generacion_documento = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
      transaccionPago.nombre_archivo = '.';
      transaccionPago.alias_archivo = '.';
      transaccionPago.path_archivo = '.';
      transaccionPago.observacion_pago = '.';
      transaccionPago.fecha_creacion = fechaActual;
      transaccionPago.usuario_creacion = usuarioId;
      transaccionPago.fecha_actualizacion = fechaActual;
      transaccionPago.usuario_actualizacion = usuarioId;
      let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
      transaccionPago.estado_id = lstEstados.filter(a => a.nemonico == 'CEGEN')[0].id_catalogo;
      let lstDetalle: any = [];
      this.listaSeleccionados.forEach(function (itm) {
        let detalle = {
          //concepto_pago_id: number;
          nemonico_concepto: 'CNSM',
          consumo_id: itm.id_consumo,
          valor_pago: itm.total,
          fecha_creacion: fechaActual,
          usuario_creacion: usuarioId,
          fecha_actualizacion: fechaActual,
          usuario_actualizacion: usuarioId,
          estado_id: lstEstados.filter(a => a.nemonico == 'ACT')[0].id_catalogo
        }
        lstDetalle.push(detalle);
      });
      transaccionPago.detalle = lstDetalle;
      //console.log(transaccionPago);
      this.transaccionPagoService.guardar(transaccionPago)
        .subscribe(resp => {
          this.listarConsumosParaPago();
          this.listarTransaccionesPago();
          this.listaSeleccionados = [];
          this.indexTab = 1;
          this.generarPdfComprobantePago(resp);
        });

    }

  }
  /**
   * Permite listar las transacciones de pago
   */
  listarTransaccionesPago() {
    if (this.personaEmpresaId != undefined) {
      this.transaccionPagoService.listarPorCodigosEstados(
        this.personaEmpresaId, '\'CEGEN\',\'RCH\'')
        .subscribe(resp => {
          this.listaTransaccionesPago = resp;
        });
    }
  }
  /**
   * Permite actualizar el estado de la transacción de pago a anulado
   * @param transaccionPago 
   */
  anularTransaccion(transaccionPago: TransaccionPago) {
    Swal.fire({
      text: "¿Esta seguro de que desea anular?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, anular!'
    }).then((result) => {
      if (result.isConfirmed) {
        let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
        let inf = {
          id_transaccion_pago: transaccionPago.id_transaccion_pago
          , fecha_actualizacion: this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss')
          , usuario_actualizacion: Number(this.sesionService.getUsuarioId())
          , estado_id: lstEstados.filter(a => a.nemonico == 'ANU')[0].id_catalogo
        };
        this.transaccionPagoService.actualizar(inf)
          .subscribe(resp => {
            this.listarConsumosParaPago();
            this.listarTransaccionesPago();
          });
      }
    });
  }
  /**
   * Permite cargar el detalle de la transacción y el detalle de pagos
   * @param transaccionPago 
   */
  seleccionFila(transaccionPago: TransaccionPago) {
    this.transaccion = new TransaccionPago();
    this.transaccion = transaccionPago;
    this.listaDetallePago = [];
    this.detalleTransaccionService.listarPorTransaccionId(this.transaccion.id_transaccion_pago.toString())
      .subscribe(resp => {
        this.valorComprobanteSeleccionado = resp.reduce((a, b) => a + Number(b.valor_pago), 0);
      });
    this.detallePagoService.listarPorTransaccionId(this.transaccion.id_transaccion_pago.toString())
      .subscribe(resp => {
        this.listaDetallePago = resp;
      });
  }
  /**
   * Permite guardar los registros que aún no estan almacenados
   */
  guardarDetallePago() {
    if (this.listaDetallePago.length == 0) {
      Swal.fire({
        position: 'top-end',
        icon: 'error',
        title: 'No hay ningún pago',
        showConfirmButton: false,
        timer: 1500
      })
    } else {
      
      if (Number(this.listaDetallePago.reduce((sum, p) => sum + Number(p.valor_pago.toString().replace(',', '.')), 0).toFixed(2))
        != Number(this.valorComprobanteSeleccionado)) {
        Swal.fire({
          position: 'top-end',
          text: "La suma de pagos difiere del valor del comprobante",
          icon: 'error',
          showConfirmButton: false,
          timer: 2000
        });
      } else {
        let lstPago = [];
        let transaccionId = this.transaccion.id_transaccion_pago;
        let fechaActual = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
        let usuarioId = this.sesionService.getUsuarioId();
        let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
        let lstTipoPago = this.listaTipoPago;
        this.listaDetallePago.forEach(function (itm) {
          if (itm.id_detalle_pago == 0) {
            let nemonicoTipoPago = lstTipoPago.filter(a => a.id_catalogo == itm.forma_pago_id)[0].nemonico;
            if (nemonicoTipoPago == 'DEP') {
              lstPago.push({
                transaccion_pago_id: transaccionId
                , forma_pago_id: itm.forma_pago_id
                , referencia: itm.referencia
                , cuenta_bancaria_id: itm.cuenta_bancaria_id
                , valor_pago: itm.valor_pago.toString().replace(',', '.')
                , fecha_creacion: fechaActual
                , usuario_creacion: usuarioId
                , fecha_actualizacion: fechaActual
                , usuario_actualizacion: usuarioId
                , estado_id: lstEstados.filter(a => a.nemonico == 'ACT')[0].id_catalogo
              });
            } else {
              lstPago.push({
                transaccion_pago_id: transaccionId
                , forma_pago_id: itm.forma_pago_id
                , banco_id: itm.banco_id
                , cuenta_bancaria: itm.cuenta_bancaria_origen
                , referencia: itm.referencia
                , cuenta_bancaria_id: itm.cuenta_bancaria_id
                , valor_pago: itm.valor_pago.toString().replace(',', '.')
                , fecha_creacion: fechaActual
                , usuario_creacion: usuarioId
                , fecha_actualizacion: fechaActual
                , usuario_actualizacion: usuarioId
                , estado_id: lstEstados.filter(a => a.nemonico == 'ACT')[0].id_catalogo
              });
            }
          }
        });
        if (lstPago.length > 0) {
          this.detallePagoService.guardar(lstPago)
            .subscribe(resp => {
              this.listaDetallePago = undefined;
              let inf = {
                id_transaccion_pago: this.transaccion.id_transaccion_pago
                , fecha_actualizacion: fechaActual
                , usuario_actualizacion: usuarioId
                , estado_id: lstEstados.filter(a => a.nemonico == 'REGI')[0].id_catalogo
              };
              this.transaccionPagoService.actualizar(inf)
                .subscribe(resp => {
                  Swal.fire({
                    icon: 'info',
                    text: "Transacción enviada exitosamente",
                    showConfirmButton: false,
                    timer: 1500
                  }
                  );
                  this.listarTransaccionesPago();
                  this.transaccion = new TransaccionPago();
                });
            });
        }
      }



    }

  }

  generarPdfComprobantePago(transaccion: TransaccionPago) {
    this.detalleTransaccionService.listarPorTransaccionId(transaccion.id_transaccion_pago.toString())
      .subscribe(resp => {
        let listaDetalleTransaccion: DetalleTransaccion[] = [];
        listaDetalleTransaccion = resp;

        let definicionDocumento = {
          content: [
            {
              text: 'JUNTA ADMINISTRADORA DE AGUA POTABLE COLAISA',
              fontSize: 16,
              alignment: 'center',
              color: '#047886'
            },
            {
              text: 'COMPROBANTE DE PAGO',
              fontSize: 14,
              bold: true,
              alignment: 'center',
              decoration: 'underline',
              color: 'skyblue'
            },
            {
              text: 'Información',
              style: 'sectionHeader'
            },
            {
              columns: [
                [
                  {
                    text: transaccion.razon_social,
                    bold: true
                  },
                  { text: transaccion.identificacion },
                  /* { text: this.invoice.email },
                  { text: this.invoice.contactNo } */
                ],
                [
                  {
                    text: `Fecha: ${new Date(transaccion.fecha_generacion_documento).toLocaleString()}`,
                    alignment: 'right'
                  },
                  {
                    text: `No : CP${transaccion.id_transaccion_pago}`,
                    alignment: 'right'
                  }
                ]
              ]
            },
            {
              text: 'Detalle',
              style: 'sectionHeader'
            },
            {
              table: {
                headerRows: 1,
                widths: ['*', '*', '*', 'auto', 'auto', 'auto', 'auto'],
                body: [
                  ['Concepto', 'Barrio', 'Medidor', 'Periodo', 'Monto', 'Recargo', 'Total'],
                  ...listaDetalleTransaccion.map(p => ([p.nombre_concepto, p.barrio, p.medidor, p.anio + '-' + p.mes, Number(p.valor_consumo).toFixed(2), Number(p.valor_mora).toFixed(2), Number(p.valor_pago).toFixed(2)])),
                  [{ text: 'Monto Total', colSpan: 6 }, {}, {}, {}, {}, {}, Number(listaDetalleTransaccion.reduce((sum, p) => sum + Number(p.valor_pago), 0)).toFixed(2)]
                ]
              }
            },
            {
              text: '',
              margin: [0, 0, 0, 15]
            },
            {
              columns: [
                [{ qr: `CP${transaccion.id_transaccion_pago}`, fit: '50' }],
                /* [{ text: 'Signature', alignment: 'right', italics: true}], */
              ]
            },
            {
              text: 'Indicaciones:',
              style: 'sectionHeader'
            },
            {
              ul: [
                'El valor del comprobante debe ser depósitado o transferido a la cuenta de ahorros No 690201017088 de la COOPERATIVA DE AHORRO Y CREDITO ANDINA.',
                'La información del depósito y/o transfrencia debe ser registrada en el sistema y enviada para su revisión y aprobación.',
                'Cuando el pago sea revisado se le notificará mediante correo electrónico el rechazo o la aprobación.',
                'Si el pago es rechazado debe realizar las respectivas correcciones y enviar nuevamente.',
                'Si el pago es aprobado se le emitirá la factura física, la misma reposará en los archivos hasta que puedan acudir a retirarla.',
              ],
            }
          ],
          styles: {
            sectionHeader: {
              bold: true,
              decoration: 'underline',
              fontSize: 14,
              margin: [0, 15, 0, 15]
            }
          }
        };

        pdfMake.createPdf(definicionDocumento).open();
      });
  }
  cambioTipoPago() {
    if (this.listaTipoPago.filter(a => a.id_catalogo == this.detallePago.forma_pago_id)[0].nemonico
      == 'DEP') {
      this.detallePago.banco_id = undefined;
      this.detallePago.cuenta_bancaria_origen = undefined;
      this.cuentaOrigenVisible = false;
    } else {
      this.cuentaOrigenVisible = true;
    }

  }
  eliminarDetallePago(r: DetallePago) {
    Swal.fire({
      text: "¿Esta seguro de que desea eliminar?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        if (r.id_detalle_pago == 0) {
          this.listaDetallePago.splice(this.listaDetallePago.indexOf(r), 1)
        } else {
          let lstPago = [];
          let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
          let fechaActual = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
          let usuarioId = this.sesionService.getUsuarioId();
          lstPago.push({
            id_detalle_pago: r.id_detalle_pago
            , fecha_actualizacion: fechaActual
            , usuario_actualizacion: usuarioId
            , estado_id: lstEstados.filter(a => a.nemonico == 'PAS')[0].id_catalogo
          });
          this.detallePagoService.guardar(lstPago)
            .subscribe(resp => {
              this.listaDetallePago.splice(this.listaDetallePago.indexOf(r), 1);
            });
        }
      }
    });
  }
  /**
   * Permite listar los socios por medio del grupo de persona y un filtro
   */
  listarSocios() {

    this.socioService.listarPorFiltro('TIPTRCCLI', this.filtroSocio)
      .subscribe(resp => {
        this.listaSocios = resp;
        //console.log(this.listaSocios);
      });
  }

  seleccionSocio(socio: Socio) {
    this.socio = new Socio();
    this.socio = socio;
    this.personaEmpresaId = this.socio.persona_empresa_id.toString();
    this.listarConsumosParaPago();
    this.listarTransaccionesPago();
    this.closebutton.nativeElement.click();
  }

  aplicarFiltroSocio() {
    //console.log('Entra al filtro');
    this.listarSocios();
  }
}
