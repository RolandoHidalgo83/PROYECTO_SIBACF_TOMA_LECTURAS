export class Consumo {

    id_consumo: bigint;
    asignacion_serie_id: bigint;
    periodo_id: bigint;
    fecha_toma_lectura: string;
    lectura_anterior: bigint;
    lectura_actual: number;
    fecha_maxima_pago: string;
    observacion: string;
    consumo_basico_id: bigint;
    excedente_id: bigint;
    umbral_id: bigint;
    valor_consumo_basico: number;
    valor_consumo_excedente: number;
    valor_consumo_umbral: number;
    valor_multa_acumulada: number;
    valor_total: number;
    abono: number;
    saldo: number;
    fecha_ultima_consulta: string;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;

}
