import { Component, OnInit } from '@angular/core';
import { RegistroConsumo } from '../../negocio/registro-consumo/registro-consumo';
import { RegistroConsumoService } from '../../negocio/registro-consumo/registro-consumo.service';
import { Periodo } from '../../administracion/periodo/periodo';
import { PeriodoService } from '../../administracion/periodo/periodo.service';
import { AprobacionLectura } from './aprobacion-lectura';
import { AprobacionLecturaService } from './aprobacion-lectura.service';
import Swal from 'sweetalert2';

import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-aprobacion-lectura',
  templateUrl: './aprobacion-lectura.component.html',
  styleUrls: ['./aprobacion-lectura.component.css']
})
export class AprobacionLecturaComponent implements OnInit {
  ubicacion = 'Aprobación de Lecturas';
  filtro: string;
  listaAnio: Periodo[];
  listaMeses: Periodo[];
  listaConsumoInicializado: RegistroConsumo[];
  anioSeleccionado: string;
  periodoSeleccionado: string;
  constructor(
    private registroConsumoService: RegistroConsumoService,
    private periodoService: PeriodoService,
    private aprobacionLecturaService: AprobacionLecturaService
  ) { }

  ngOnInit(): void {
    this.listarAnios();
  }
  /**
   * Permite listar los años disponibles
   */
  listarAnios(): void {
    this.periodoService.listarAnio()
      .subscribe(resp => {
        this.listaAnio = resp;
      });
  }
  /**
   * Permite listar los periodos por medio del año
   */
  cambioAnio() {
    this.listarMeses();
  }
  /**
   * Permite listar los periodos por medio del año y el nemónico del estado
   */
  listarMeses(): void {
    this.periodoService.listarPorAnio(this.anioSeleccionado, 'ACT')
      .subscribe(resp => {
        this.listaMeses = resp;
      });
  }
  /**
     * Permite listar los consumos con estado generado
     */
  cambioPeriodo() {
    if (!this.periodoSeleccionado) {
      Swal.fire({
        icon: 'error',
        title: 'Seleccione un periodo',
        showConfirmButton: false,
        timer: 1000
      });
    } else {
      this.registroConsumoService.listarConsumoInicializado(
        this.periodoSeleccionado, 'REGI', '0', this.filtro)
        .subscribe(resp => {
          this.listaConsumoInicializado = resp;
          this.listaConsumoInicializado.forEach(function (itm) {
            itm.seleccionado = true
          });
        });
    }
  }
  aprobarLecturas() {
    let listaIds: AprobacionLectura[] = [];
    let ids = [];
    this.listaConsumoInicializado.filter(a => a.seleccionado)
    .forEach(function (itm) {
        let id = { id_consumo: itm.id_consumo };
        ids.push(id);
      })
    listaIds = ids;
    this.aprobacionLecturaService.aprobarLectura(listaIds)
    .subscribe(resp => {
      this.cambioPeriodo();
    });
  }
  generarPdfLecturaAprobada(){
    let listaConsumoAprobado: RegistroConsumo[] = [];
    this.registroConsumoService.listarConsumoInicializado(
      this.periodoSeleccionado, 'APRV', '0', '')
      .subscribe(resp => {
        listaConsumoAprobado = resp;
        let definicionDocumento = {
          header: {
            margin: 20,
            columns: [
              
              {
                //margin: [10, 0, 0, 0],
                text: 'JUNTA ADMINISTRADORA DE AGUA POTABLE COLAISA',
                fontSize: 16,
                alignment: 'center',
                color: '#047886'
              }
              /* ,
              {
                text: 'APROBACIÓN DE LECTURAS',
                fontSize: 14,
                bold: true,
                alignment: 'center',
                //decoration: 'underline',
                color: 'skyblue'
              } */
            ],
            
          },
          content: [
            /* {
              text: 'JUNTA ADMINISTRADORA DE AGUA POTABLE COLAISA',
              fontSize: 16,
              alignment: 'center',
              color: '#047886'
            }, */
            {
              text: 'APROBACIÓN DE LECTURAS',
              fontSize: 14,
              bold: true,
              alignment: 'center',
              decoration: 'underline',
              color: 'skyblue'
            },
            {
              table: {
                headerRows: 1,
                widths: ['auto', '*', 'auto', 'auto', 'auto', 'auto', 'auto'],
                body: [
                  ['C.I./RUC', 'Nombres', 'Barrio', 'Medidor', 'L. Ant.', 'L. Act.', 'Con.'],
                  ...listaConsumoAprobado.map(p => ([p.identificacion, p.razon_social, p.barrio, p.codigo, Number(p.lectura_anterior).toFixed(0), Number(p.lectura_actual).toFixed(0), Number(p.consumo).toFixed(0)])),
                  //[{ text: 'Monto Total', colSpan: 6 }, {}, {}, {}, {}, {}, Number(listaDetalleTransaccion.reduce((sum, p) => sum + Number(p.valor_pago), 0)).toFixed(2)]
                ]
              }
            },
            {
              text: '',
              margin: [0, 0, 0, 50]
            },
            {
              columns: [
                {
                  text: '___________________________________',
                },
                {
                  text: '___________________________________',
                }
              ]
            },
            {
              columns: [
                {
                  text: 'Responsable: ____________________',
                },
                {
                  text: 'Aprobado por: ____________________',
                }
              ]
            }
          ],
          styles: {
            sectionHeader: {
              bold: true,
              decoration: 'underline',
              fontSize: 14,
              margin: [0, 15, 0, 15]
            }
          }
        };
    
        pdfMake.createPdf(definicionDocumento).open();
      });
      
    
  }
}
