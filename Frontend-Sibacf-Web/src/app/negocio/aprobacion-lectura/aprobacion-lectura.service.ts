import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UtilitarioService} from '../../servicio/utilitario.service';


@Injectable({
  providedIn: 'root'
})
export class AprobacionLecturaService {
  private endPointAprobarLectura: string = '/consumo/aprobarLecturasPorIds';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * 
   * @param objeto Permite aprobar y calcular el consumo de lecturas por medios de ids de consumo
   */
aprobarLectura(objeto: Object): Observable<any>
{
  return this.http.post(`${this.utilitarioService.getURL()}${this.endPointAprobarLectura}`, objeto);
}

}
