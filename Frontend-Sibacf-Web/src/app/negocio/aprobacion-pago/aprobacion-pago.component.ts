import { Component, OnInit } from '@angular/core';
import { SesionService } from '../../servicio/sesion.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Catalogo } from '../../administracion/catalogo/catalogo';
import { TransaccionPago } from '../transaccion-pago/transaccion-pago';
import { DatePipe } from '@angular/common';
import { TransaccionPagoService } from '../transaccion-pago/transaccion-pago.service';
import { DetallePago } from '../detalle-pago/detalle-pago';
import { DetallePagoService } from '../detalle-pago/detalle-pago.service';

@Component({
  selector: 'app-aprobacion-pago',
  templateUrl: './aprobacion-pago.component.html',
  styleUrls: ['./aprobacion-pago.component.css'],
  providers: [DatePipe]
})
export class AprobacionPagoComponent implements OnInit {
  ubicacion: string = 'Aprobación de Pagos';
  listaTransaccionesPago: TransaccionPago[];
  transaccionPago: TransaccionPago = new TransaccionPago();
  listaDetallePago: DetallePago[];
  totalPagos: number;
  aprobarComprobante: boolean;
  constructor(
    private sesionService: SesionService,
    private router: Router,
    private transaccionPagoService: TransaccionPagoService,
    private detallePagoService: DetallePagoService,
    private datePipe: DatePipe,
  ) { }

  ngOnInit(): void {
    if (this.sesionService.getUsuarioId()) {
      this.listarTransaccionesPago();
    } else {
      this.router.navigate(['./login']);
    }
  }
  /**
   * Permite listar las transacciones de pago
   */
  listarTransaccionesPago() {
    this.transaccionPagoService.listarPorCodigosEstados('0', '\'REGI\'')
      .subscribe(resp => {
        this.listaTransaccionesPago = resp;
      });
  }
  /**
   * Evento selección del registro
   */
  seleccionRegistroPago(transaccoin: TransaccionPago) {
    this.transaccionPago = transaccoin;
    this.detallePagoService.listarPorTransaccionId(this.transaccionPago.id_transaccion_pago.toString())
      .subscribe(resp => {
        this.listaDetallePago = resp;
        this.totalPagos = resp.reduce((a, b) => a + Number(b.valor_pago), 0);
        this.aprobarComprobante = true;
      })
  }
  /**
   * Permite modificar el estado de la transacción a aprobado o negado
   */
  actualizarEstadoTransaccion() {
    if (this.transaccionPago.id_transaccion_pago == undefined) {
      Swal.fire({
        icon: 'error',
        text: "Seleccione un comprobante",
        showConfirmButton: false,
        timer: 1500
      }
      );
    } else {
      if (this.aprobarComprobante == undefined) {
        Swal.fire({
          icon: 'error',
          text: "Seleccione un estado",
          showConfirmButton: false,
          timer: 1500
        }
        );
      } else {
        let continuar: boolean = true;
        if (!this.aprobarComprobante) {
          if (!this.transaccionPago.observacion_pago
            || !this.transaccionPago.observacion_pago.trim()
          ) {
            continuar = false;
            Swal.fire({
              icon: 'error',
              text: "Especifique una observación",
              showConfirmButton: false,
              timer: 1500
            }
            );
          }
        } else {
          if (Number(this.listaDetallePago.reduce((sum, p) => sum + Number(p.valor_pago.toString().replace(',', '.')), 0).toFixed(2))
            != Number(this.transaccionPago.valor_comprobante)) {
            continuar = false;
            Swal.fire({
              icon: 'error',
              text: "La suma de las formas de pago difieren del valor del comprobante de pago",
              showConfirmButton: false,
              timer: 1500
            }
            );
          }
        }
        if (continuar) {
          let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
          let inf = {
            id_transaccion_pago: this.transaccionPago.id_transaccion_pago
            , fecha_actualizacion: this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss')
            , usuario_actualizacion: Number(this.sesionService.getUsuarioId())
            , estado_id: lstEstados.filter(a => a.nemonico == (this.aprobarComprobante ? 'APRV' : 'RCH'))[0].id_catalogo
            , observacion_pago: (
              this.transaccionPago.observacion_pago == undefined ? '.' :
                (this.transaccionPago.observacion_pago.trim() == '' ? '.' : this.transaccionPago.observacion_pago)
            )
          };
          this.transaccionPagoService.actualizar(inf)
            .subscribe(resp => {
              Swal.fire({
                icon: 'info',
                text: "Transacción guardada exitosamente",
                showConfirmButton: false,
                timer: 1500
              }
              );
              this.listaDetallePago = [];
              this.totalPagos = 0;
              this.transaccionPago = new TransaccionPago();
              this.aprobarComprobante = true;
              this.listarTransaccionesPago();
            });
        }
      }
    }
  }
}
