import { DetalleTransaccion } from '../detalle-transaccion/detalle-transaccion';

export class TransaccionPago {
    id_transaccion_pago: number;
    empresa_id: number;
    origen_pago_id: number;
    numero_documento: string;
    fecha_generacion_documento: string;
    nombre_archivo: string;
    alias_archivo: string;
    path_archivo: string;
    observacion_pago: string;
    identificacion: string;
    razon_social: string;
    valor_comprobante: number;
    fecha_creacion: string;
    usuario_creacion: number;
    fecha_actualizacion: string;
    usuario_actualizacion: number;
    estado_id: number;
    estado: string;
    detalle: DetalleTransaccion [];
    /* [
        {
            id_detalle_transaccion_pago: number;
            transaccion_pago_id: number;
            concepto_pago_id: number;
            consumo_id: number;
            valor_pago: number;
            fecha_creacion: string;
            usuario_creacion: number;
            fecha_actualizacion: string;
            usuario_actualizacion: number;
            estado_id: number;
        }
    ] */
}
