import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import { TransaccionPago } from './transaccion-pago';

@Injectable({
  providedIn: 'root'
})
export class TransaccionPagoService {
  private endPointListarNoAnulados: string = '/transaccionPago/getTransaccionPagoPorPersonaEmpresaIdPorCodigosEstado';
  private endPointGuardar: string = '/transaccionPago/saveTransaccionPago';
  private endPointActualizar: string = '/transaccionPago/updateTransaccionPago';

  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * Permite grabar la transacción y el detalle
   * @param objeto 
   */
  guardar(objeto: Object): Observable<any> {
    return this.http.post(`${this.utilitarioService.getURL()}${this.endPointGuardar}`, objeto);
  }
  /**
   * Permite listar las transacciones por medio de los códigos de los estados
   * @param codigosEstados Códigos separados por coma
   */
  listarPorCodigosEstados(personaEmpresaId: string, codigosEstados: string): Observable<TransaccionPago[]> {
    return this.http.get<TransaccionPago[]>(
      `${this.utilitarioService.getURL()}${this.endPointListarNoAnulados}/${personaEmpresaId}/${codigosEstados}`);
  }
  /**
   * Permite actualizar el estado de la transacción de pago
   * @param objeto 
   */
  actualizar(objeto: Object): Observable<any>{
    return this.http.post(`${this.utilitarioService.getURL()}${this.endPointActualizar}`, objeto);
  }
}
