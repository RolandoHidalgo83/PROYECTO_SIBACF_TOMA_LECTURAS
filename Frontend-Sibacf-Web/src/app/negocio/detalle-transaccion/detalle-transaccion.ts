export class DetalleTransaccion {
    id_detalle_transaccion_pago: number;
    transaccion_pago_id: number;
    concepto_pago_id: number;
    nemonico_concepto: string;
    nombre_concepto: string;
    consumo_id: number;
    barrio: string;
    medidor: string;
    anio: string;
    mes: string;
    valor_consumo: number;
    valor_mora: number;
    valor_pago: number;
    fecha_creacion: string;
    usuario_creacion: number;
    fecha_actualizacion: string;
    usuario_actualizacion: number;
    estado_id: number;
}
