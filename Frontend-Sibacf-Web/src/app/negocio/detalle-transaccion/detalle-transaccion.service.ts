import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import { DetalleTransaccion } from './detalle-transaccion';

@Injectable({
  providedIn: 'root'
})
export class DetalleTransaccionService {
  private endPointListarPorTransaccionId: string = '/detalleTransaccionPago/getDetalleTransaccionPagoPorTransaccionId';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * Permite listar por medio del id de la transacción
   * @param transaccionId 
   */
  listarPorTransaccionId(transaccionId: string): Observable<DetalleTransaccion[]>
  {
    return this.http.get<DetalleTransaccion[]>(`${this.utilitarioService.getURL()}${this.endPointListarPorTransaccionId}/${transaccionId}`);
  }
}
