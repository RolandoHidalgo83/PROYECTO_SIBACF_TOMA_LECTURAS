import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { AsignacionMedidorService } from '../../administracion/asignacion-medidor/asignacion-medidor.service';
import { SesionService } from '../../servicio/sesion.service';
import { AsignacionMedidor } from '../../administracion/asignacion-medidor/asignacion-medidor';
import { SerieService } from '../../administracion/serie/serie.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-catastro',
  templateUrl: './catastro.component.html',
  styleUrls: ['./catastro.component.css'],
  providers: [DatePipe]
})
export class CatastroComponent implements OnInit {
  listaCatastro: AsignacionMedidor[];
  ubicacion: string = "Gestión de Catastro";
  filtro: string;
  @ViewChild("txtSerie") elementoSerie: ElementRef;
  @ViewChild("txtOrden") elementoOrden: ElementRef;
  constructor(private asignacionMedidorService: AsignacionMedidorService,
    private serieService: SerieService,
    private sesionService: SesionService,
    private router: Router,
    private datePipe: DatePipe,
  ) { }

  ngOnInit(): void {
    if (this.sesionService.getUsuarioId()) {
      this.listarPorFiltro();
    } else {
      this.router.navigate(['./login']);
    }
  }
  /**
   * Permite listar los registros activos por medio de un filtro
   */
  listarPorFiltro() {
    this.asignacionMedidorService.obtenerActivoPorFiltro(this.filtro)
      .subscribe(resp => {
        this.listaCatastro = resp;
      });
  }
  ponerFocoEnCampoOrden() {
    this.elementoOrden.nativeElement.focus();
  }
  grabar() {
    this.elementoOrden.nativeElement.focus();
  }

  guardarSerie(reg: AsignacionMedidor) {
    if (reg.serie == undefined) {
      Swal.fire({
        icon: 'error',
        text: "Especifique la serie",
        showConfirmButton: false,
        timer: 1500
      }
      );
    } else {
      if (reg.serie.trim() == "") {
        Swal.fire({
          icon: 'error',
          text: "Especifique la serie",
          showConfirmButton: false,
          timer: 1500
        }
        );
      } else {
        let inf = {
          id_serie: reg.serie_id
          , serie: reg.serie
          , fecha_actualizacion: this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss')
          , usuario_actualizacion: Number(this.sesionService.getUsuarioId())
        }
        this.serieService.guardarUnRegistro(inf)
          .subscribe(resp => {
            Swal.fire({
              icon: 'success',
              showConfirmButton: false,
              timer: 1000
            });
          });
      }
    }
  }
  guardarOrden(reg: AsignacionMedidor) {
    if (reg.orden == undefined) {
      Swal.fire({
        icon: 'error',
        text: "Especifique un número de orden",
        showConfirmButton: false,
        timer: 1500
      }
      );
    } else {
      if (reg.orden == 0) {
        Swal.fire({
          icon: 'error',
          text: "Especifique un número de orden",
          showConfirmButton: false,
          timer: 1500
        }
        );
      } else {
        let inf = {
          id_asignacion_serie: reg.id_asignacion_serie
          , orden: reg.orden
          , fecha_actualizacion: this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss')
          , usuario_actualizacion: Number(this.sesionService.getUsuarioId())
        }
        this.asignacionMedidorService.guardarUnRegistro(inf)
          .subscribe(resp => {
            Swal.fire({
              icon: 'success',
              showConfirmButton: false,
              timer: 1000
            });
          });
      }
    }
  }

}
