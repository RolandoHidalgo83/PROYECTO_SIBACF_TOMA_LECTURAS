export class RegistroConsumo {
    id_consumo: number
    identificacion: string;
    razon_social: string;
    item: string;
    codigo: string;
    barrio: string;
    anio: string;
    mes: string;
    fecha_toma_lectura: string;
    lectura_anterior: number;
    lectura_actual: number;
    fecha_maxima_pago: string;
    consumo: number;
    valor_consumo_basico: number;
    valor_excedente: number;
    valor_adicional: number;
    sub_total: number;
    valor_mora: number;
    total: number;
    saldo: number;
    seleccionado: boolean;
}
