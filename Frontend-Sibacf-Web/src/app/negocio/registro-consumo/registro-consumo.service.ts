import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UtilitarioService} from '../../servicio/utilitario.service';
import { RegistroConsumo } from './registro-consumo';

@Injectable({
  providedIn: 'root'
})
export class RegistroConsumoService {
  private endPointConsultaParaToma: string = '/consumo/getConsumoInicializado';
  private endPointMultiActualizar: string = '/consumo/multiUpdateConsumo';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * Permite listar los registros de consumo con estado generado
   * @param periodoId 
   * @param filtro 
   */
  listarConsumoInicializado(periodoId: string, nemonicoEstado: string, personaEmpresaId: string, filtro: string):Observable<RegistroConsumo[]>
  {
    if(!filtro){
      filtro = '@';
    }
    //console.log(`${this.utilitarioService.getURL()}${this.endPointConsultaParaToma}/${periodoId}/${nemonicoEstado}/${personaEmpresaId}/${filtro}`);
    return this.http.get<RegistroConsumo[]>(`${this.utilitarioService.getURL()}${this.endPointConsultaParaToma}/${periodoId}/${nemonicoEstado}/${personaEmpresaId}/${filtro}`);
  }
  /**
   * Permite actualizar uno o varios registros
   * @param obejto 
   */
  multiActualizar(obejto: object):Observable<any>
  {
    return this.http.post(`${this.utilitarioService.getURL()}${this.endPointMultiActualizar}`, obejto);
  }

}
