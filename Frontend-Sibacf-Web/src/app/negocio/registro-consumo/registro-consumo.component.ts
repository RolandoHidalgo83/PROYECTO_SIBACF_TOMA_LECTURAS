import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { SesionService } from '../../servicio/sesion.service';
import { Periodo } from '../../administracion/periodo/periodo';
import { PeriodoService } from '../../administracion/periodo/periodo.service';
import { RegistroConsumoService } from './registro-consumo.service';
import { RegistroConsumo } from './registro-consumo';
import { Router } from '@angular/router';
import { from } from 'rxjs';
import Swal from 'sweetalert2';
import { Catalogo } from '../../administracion/catalogo/catalogo';

@Component({
  selector: 'app-registro-consumo',
  templateUrl: './registro-consumo.component.html',
  styleUrls: ['./registro-consumo.component.css'],
  providers: [DatePipe]
})
export class RegistroConsumoComponent implements OnInit {
  ubicacion: string = 'Registro de consumos';
  listaAnio: Periodo[];
  listaPeriodos: Periodo[];
  anioSeleccionado: string;
  periodoSeleccionado: string;
  listaConsumoInicializado: RegistroConsumo[] = [];
  filtro: string;
  estadoGenerado: boolean;
  cargando: boolean;
  constructor(
    private periodoService: PeriodoService,
    private sesionService: SesionService,
    private router: Router,
    private registroConsumoService: RegistroConsumoService,
    private datePipe: DatePipe,
  ) { }

  ngOnInit(): void {
    if (this.sesionService.getUsuarioId()) {
      this.estadoGenerado = true;
      this.listarAnios();
      this.cargando = false;
    } else {
      this.router.navigate(['./login']);
    }
  }
  /**
   * Permite listar los años disponibles
   */
  listarAnios(): void {
    this.periodoService.listarAnio()
      .subscribe(resp => {
        this.listaAnio = resp;
      });
  }
  /**
   * Permite listar los periodos por medio del año seleccionado
   */
  cambioAnio() {
    this.listarMeses();
  }
  /**
   * Permite listar los periodos por medio del año y el nemónico del estado
   */
  listarMeses(): void {
    this.periodoService.listarPorAnio(this.anioSeleccionado, 'ACT')
      .subscribe(resp => {
        this.listaPeriodos = resp;
      });
  }
  /**
   * Permite listar los consumos con estado generado
   */
  cambioPeriodo() {
    this.cargando = true;
    if (!this.periodoSeleccionado) {
      Swal.fire({
        icon: 'error',
        title: 'Seleccione un periodo',
        showConfirmButton: false,
        timer: 1000
      });
    } else {
      this.listarParaRegistroDeLectura();
    }
  }

  listarParaRegistroDeLectura() {
    this.registroConsumoService.listarConsumoInicializado(
      this.periodoSeleccionado, (this.estadoGenerado ? 'CEGEN' : 'REGI'), '0', this.filtro)
      .subscribe(resp => {
        this.listaConsumoInicializado = resp;
        this.listaConsumoInicializado.forEach(function (itm) {
          if (itm.lectura_actual == 0) {
            itm.lectura_actual = itm.lectura_anterior;
          }
        });
        this.cargando = false;

      });
  }

  guardarRegistro(registro: RegistroConsumo) {
    registro.consumo = registro.lectura_actual - registro.lectura_anterior;
    let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
    let lst = [];
    let inf = {
      id_consumo: registro.id_consumo
      , lectura_anterior: registro.lectura_anterior
      , lectura_actual: registro.lectura_actual
      , fecha_actualizacion: this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss')
      , usuario_actualizacion: Number(this.sesionService.getUsuarioId())
      , estado_id: lstEstados.filter(a => a.nemonico == 'REGI')[0].id_catalogo
    };
    lst.push(inf);
    this.registroConsumoService.multiActualizar(lst)
      .subscribe(resp => {
        if (this.estadoGenerado) {
          this.listaConsumoInicializado.splice(this.listaConsumoInicializado.indexOf(registro), 1);
        }else{
          Swal.fire({
            icon: 'info',
            title: 'Lectura registrada exitosamente',
            showConfirmButton: false,
            timer: 1500
          });
        }
      });



  }
  /**
   * Permite guardar toda la lista
   */
  guardarTodo() {
    if (this.listaConsumoInicializado.length != 0) {

      Swal.fire({
        text: "¿Está seguro de que desea registrar la lectura?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, registrar!'
      }).then((result) => {
        if (result.isConfirmed) {
          let lst = [];
          let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
          let usuarioId = Number(this.sesionService.getUsuarioId());
          let fechaActual = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
          this.listaConsumoInicializado
            .filter(a => a.lectura_actual != null && a.lectura_actual != 0)
            .forEach(function (itm) {
              itm.consumo = itm.lectura_actual - itm.lectura_anterior;
              let inf = {
                id_consumo: itm.id_consumo
                , lectura_anterior: itm.lectura_anterior
                , lectura_actual: itm.lectura_actual
                , fecha_actualizacion: fechaActual
                , usuario_actualizacion: usuarioId
                , estado_id: lstEstados.filter(a => a.nemonico == 'REGI')[0].id_catalogo
              };
              lst.push(inf);
            });
          if (lst.length > 0) {
            this.registroConsumoService.multiActualizar(lst)
              .subscribe(resp => {
                this.listarParaRegistroDeLectura();
                /* if (this.estadoGenerado == true) {
                  this.listaConsumoInicializado = null;
                } */
                Swal.fire({
                  icon: 'info',
                  title: 'Registros guardados exitosamente',
                  showConfirmButton: false,
                  timer: 1500
                });
              });
          }
        }
      });
    }
  }

  calcularConsumo(registro: RegistroConsumo) {
    if (Number(registro.lectura_actual) - Number(registro.lectura_anterior) < 0) {
      registro.consumo = 0;
    } else {
      registro.consumo = Number(registro.lectura_actual) - Number(registro.lectura_anterior);
    }
  }
}
