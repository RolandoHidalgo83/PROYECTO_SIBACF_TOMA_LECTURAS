export class DetallePago {
    id_detalle_pago: number;
    transaccion_pago_id: number;
    forma_pago_id: number;
    forma_pago: string;
    banco_id: number;
    banco_origen: string;
    cuenta_bancaria_origen: string;
    cuenta_bancaria_id?: number;
    banco_destino: string;
    cuenta_bancaria_destino: string;
    referencia: string;
    valor_pago: number;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: number;
}
