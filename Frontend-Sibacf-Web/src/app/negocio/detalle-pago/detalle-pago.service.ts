import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import {DetallePago } from './detalle-pago';

@Injectable({
  providedIn: 'root'
})
export class DetallePagoService {
  private endPointListarPorTransaccionId: string = '/detallePago/getDetallePagoPorTransaccionId';
  private endPointGrabar: string = '/detallePago/saveDetallePago';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * Permite listar por medio del id de la transacción
   * @param transaccionId 
   */
  listarPorTransaccionId(transaccionId: string): Observable<DetallePago[]>
  {
    return this.http.get<DetallePago[]>(`${this.utilitarioService.getURL()}${this.endPointListarPorTransaccionId}/${transaccionId}`);
  }
  /**
   * Permite grabar la transacción y el detalle
   * @param objeto 
   */
  guardar(objeto: Object): Observable<any> {
    return this.http.post(`${this.utilitarioService.getURL()}${this.endPointGrabar}`, objeto);
  }

}
