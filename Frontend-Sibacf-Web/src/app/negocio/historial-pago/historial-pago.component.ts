import { Component, OnInit } from '@angular/core';
import { SesionService } from '../../servicio/sesion.service';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { Catalogo } from '../../administracion/catalogo/catalogo';
import { DatePipe } from '@angular/common';
import { TransaccionPagoService } from '../transaccion-pago/transaccion-pago.service';
import { TransaccionPago } from '../transaccion-pago/transaccion-pago';

@Component({
  selector: 'app-historial-pago',
  templateUrl: './historial-pago.component.html',
  styleUrls: ['./historial-pago.component.css'],
  providers: [DatePipe]
})
export class HistorialPagoComponent implements OnInit {
  ubicacion: string = 'Historial de Pagos';
  listaTransaccionesPago: TransaccionPago[];
  constructor(
    private sesionService: SesionService,
    private router: Router,
    private datePipe: DatePipe,
    private transaccionPagoService: TransaccionPagoService
  ) { }

  ngOnInit(): void {
    if (this.sesionService.getUsuarioId()) {
      this.listarPagosRealizados();
    } else {
      this.router.navigate(['./login']);
    }
  }
  listarPagosRealizados(){
    this.transaccionPagoService.listarPorCodigosEstados('0', '\'APRV\'')
    .subscribe(resp => {
      this.listaTransaccionesPago = resp;
    });
  }
}
