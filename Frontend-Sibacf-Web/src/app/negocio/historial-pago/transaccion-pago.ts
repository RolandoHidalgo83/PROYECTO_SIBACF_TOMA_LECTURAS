export class TransaccionPago {

    id_transaccion_pago: bigint;
    empresa_id: bigint;
    origen_pago_id: bigint;
    numero_documento: string;
    fecha_generacion_documento: string;
    nombre_archivo: string;
    alias_archivo: string;
    path_archivo: string;
    observacion_pago: string;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;
    
}
