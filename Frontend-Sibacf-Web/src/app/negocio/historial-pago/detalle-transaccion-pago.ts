export class DetalleTransaccionPago {

    id_detalle_transaccion_pago: bigint;
    transaccion_pago_id: bigint;
    concepto_pago_id: bigint;
    consumo_id: bigint;
    valor_pago: number;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;
}
