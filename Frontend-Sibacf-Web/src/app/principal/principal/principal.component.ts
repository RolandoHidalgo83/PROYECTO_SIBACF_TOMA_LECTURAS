import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SesionService } from '../../servicio/sesion.service';
import { Menu} from '../../modelo/menu';
import { RolAccesoService} from '../../seguridades/rol-acceso/rol-acceso.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {
  empresa: string = 'JUNTA ADMINISTRADORA DE AGUA POTABLE COLAISA';
  usuario: string;
  isLogged: boolean = false;
  listaAccesos: Menu[];
  constructor(
    private router: Router,
    private sesionService: SesionService,
    private rolAccesoService: RolAccesoService
  ) { }

  ngOnInit(): void {
    this.usuario = "";
    console.log(this.sesionService.getUsuarioId());
    if (this.sesionService.getUsuarioId()) {
      this.usuario = 'Usuario: ' + this.sesionService.getLogin();
      this.isLogged = true;
      this.rolAccesoService.listarAccesosActivosPorUsuarioAutenticado()
      .subscribe(resp =>{
        
        let menus: Menu[] = resp; 
        let con: number = 1;
        let info = [];
        menus.forEach(function(men){
          if(men.acceso_padre_id == undefined){
            info.push({texto: men.texto, icono: men.icono, id_acceso: men.id_acceso, accion: men.accion});
            con++;
          }
        });
        this.listaAccesos = info;
        let total = menus.length;
        while(con <= total) {          
          this.listaAccesos.forEach(function(item){
            let itm = [];
            menus.forEach(function(men){
              if(men.acceso_padre_id != undefined && men.acceso_padre_id == item.id_acceso) {
                let info = {texto: men.texto, icono: men.icono, id_acceso: men.id_acceso, accion: men.accion};              
                
                itm.push(info);
                menus = menus.filter(itm => itm.id_acceso != men.id_acceso);
                con++;
              }
            });
            item['menuHijo'] = itm;
          });
        }
      });
    } else {
      this.isLogged = false;
    }
  }

  /**
  * Cierra session
 */
  public cerrarSesion(): void {
    this.sesionService.cerrarSesion();
    this.isLogged = false;
    window.location.reload();

  }
}
