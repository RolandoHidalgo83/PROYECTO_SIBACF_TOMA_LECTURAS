import { Component, Input, OnInit, ViewChild } from '@angular/core';
import {Router} from '@angular/router';
import { Menu } from '../../../modelo/menu';

@Component({
  selector: 'app-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.css']
})
export class SubMenuComponent implements OnInit {
  @Input() items: Menu[];
  //@ViewChild('childMenu') public childMenu;
  @ViewChild('childMenu', {static: true}) public childMenu: any;
  constructor(public router: Router) { }

  ngOnInit(): void {
  }

}
