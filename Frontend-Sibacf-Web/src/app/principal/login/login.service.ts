import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { observable, Observable } from 'rxjs';
import { UsuarioAutenticado } from './usuario-autenticado';
import { UtilitarioService} from '../../servicio/utilitario.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private endPoint: string = '/usuario/getUsuarioLoginPass';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService

  ) { }

  obtenerUsuario(usr : string, pass: string): Observable<UsuarioAutenticado>
  {
    //console.log('HOLA URL: ' + `${this.utilitarioService.getURL()}${this.endPoint}/${usr}/${pass}`);
    
    return this.http.get<UsuarioAutenticado>(`${this.utilitarioService.getURL()}${this.endPoint}/${usr}/${pass}`);
  }
}
