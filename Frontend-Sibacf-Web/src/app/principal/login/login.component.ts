import { Component, OnInit } from '@angular/core';
import { UsuarioAutenticado } from './usuario-autenticado'
import { LoginService } from './login.service';
import Swal from 'sweetalert2';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { SesionService } from '../../servicio/sesion.service';
import { CatalogoService } from '../../administracion/catalogo/catalogo.service';
import { Catalogo } from '../../administracion/catalogo/catalogo';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  usr: string;
  pass: string;
  //usuario: Usuario = new Usuario();
  private isLogged: boolean = false;

  constructor(
    private loginService: LoginService,
    private utilitarioService: UtilitarioService,
    private sesionService: SesionService,
    private catalogoService: CatalogoService,
    private router: Router
  ) { }
  ngOnInit(): void {
    if (this.sesionService.getUsuarioId()) {
      this.isLogged = true;
      //this.router.navigate(['./inicio']);
    } else {
      this.isLogged = false;
    }
  }
  /**
   * Metodo que se encarga de autenticar 
   */
  login(): void {
    if (this.usr === undefined || this.pass === undefined) {
      Swal.fire(
        'Error',
        `Especifique un usuario y una contraseña`,
        'error'
      );
    } else {
      
      this.loginService.obtenerUsuario(this.usr, this.utilitarioService.getDatoEncriptadoEnMD5(this.pass).toString())
        .subscribe(resp => {
          if (resp.id_usuario === undefined) {
            Swal.fire('ERROR', `Usuario o contraseña incorrectos.`, 'error');
          } else {
            if (resp.confirmacion) {
              this.isLogged = true;
              //let usuarioAutenticado: UsuarioAutenticado = resp;
              this.sesionService.setUsuario(resp);
              this.catalogoService.obtenerPorNemonicoGrupo('EST')
                .subscribe(r => {
                  this.sesionService.setEstados(r)
                });
              Swal.fire({
                icon: 'success',
                title: 'BIENVENIDO',
                showConfirmButton: false,
                timer: 800
              }
              ).then((result) => {

                window.location.reload();

              })
              this.router.navigate(['./inicio']);
            }else{
              this.router.navigate(['./cambioClave/' + resp.id_usuario]);
            }
          }
        },
          err => {
            Swal.fire('ERROR', `Usuario o contraseña incorrectos.`, 'error');
          }
        );
    }
  }


}
