export class Usuario {
    id_usuario: bigint;
    persona_empresa_id: bigint;
    login: string;
    pass: string;
    confirmacion: boolean;
    fecha_expiracion: string;
    fecha_ultimo_acceso: string;
    sesion_activa: boolean;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;
    
}
