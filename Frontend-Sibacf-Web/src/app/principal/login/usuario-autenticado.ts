export class UsuarioAutenticado {
    id_usuario: number;
    usuario: string;
    tipo_usuario_id: number;
    nemonico_tipo_usuario: string;
    id_persona_empresa: number;
    id_persona: number;
    persona: string;
    id_empresa: number;
    empresa: string;
    confirmacion: boolean;
}
