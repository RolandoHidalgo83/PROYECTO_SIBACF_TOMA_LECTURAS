import { Component, OnInit } from '@angular/core';
import { SesionService } from '../../servicio/sesion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  private isLogged: boolean = false;
  constructor(
    private sesionService: SesionService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this.sesionService.getUsuarioId()) {
      this.isLogged = true;
    } else {
      this.isLogged = false;
      this.router.navigate(['./login']);
    }
  }

}
