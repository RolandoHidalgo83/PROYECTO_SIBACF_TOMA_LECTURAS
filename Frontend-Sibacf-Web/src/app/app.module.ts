import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from "@angular/forms";
import { AppComponent } from './app.component';
import { PrincipalComponent } from './principal/principal/principal.component';
import { LoginComponent } from './principal/login/login.component';
import { HttpClientModule } from '@angular/common/http';
import { InicioComponent } from './principal/inicio/inicio.component';
import { RolComponent } from './seguridades/rol/rol.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RolAccesoComponent } from './seguridades/rol-acceso/rol-acceso.component';
import { AccesoComponent } from './seguridades/acceso/acceso.component';
import { SubMenuComponent } from './principal/principal/sub-menu/sub-menu.component';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { RegistroConsumoComponent } from './negocio/registro-consumo/registro-consumo.component';
import { PagoComponent } from './negocio/pago/pago.component';
import { HistorialPagoComponent } from './negocio/historial-pago/historial-pago.component';
import { NotificacionComponent } from './negocio/notificacion/notificacion.component';
import { UsuarioComponent } from './seguridades/usuario/usuario.component';
import { AsignacionMedidorComponent } from './administracion/asignacion-medidor/asignacion-medidor.component';
import { BancoComponent } from './administracion/banco/banco.component';
import { CuentaBancariaComponent } from './administracion/cuenta-bancaria/cuenta-bancaria.component';
import { GrupoParametroComponent } from './administracion/grupo-parametro/grupo-parametro.component';
import { ItemComponent } from './administracion/item/item.component';
import { ParametroComponent } from './administracion/parametro/parametro.component';
import { PeriodoComponent } from './administracion/periodo/periodo.component';
import { SerieComponent } from './administracion/serie/serie.component';
import { SocioComponent } from './administracion/socio/socio.component';
import { AprobacionLecturaComponent } from './negocio/aprobacion-lectura/aprobacion-lectura.component';
import { AprobacionPagoComponent } from './negocio/aprobacion-pago/aprobacion-pago.component';
import {CalendarModule} from 'primeng/calendar';
import {PanelModule} from 'primeng/panel';
import {TableModule} from 'primeng/table';
import {FieldsetModule} from 'primeng/fieldset';
import {DropdownModule} from 'primeng/dropdown';
import {InputNumberModule} from 'primeng/inputnumber';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatInputModule } from '@angular/material/input';
import { UsuarioRolComponent } from './seguridades/usuario-rol/usuario-rol.component';
import { CambioClaveComponent } from './seguridades/cambio-clave/cambio-clave.component';
import { FlexLayoutModule } from "@angular/flex-layout";
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { CatastroComponent } from './negocio/catastro/catastro.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: 'login', component: LoginComponent },
  { path: 'inicio', component: InicioComponent },
  { path: 'cambioClave/:usuarioId', component: CambioClaveComponent },
  { path: 'asignacionMedidores', component: AsignacionMedidorComponent },
  { path: 'bancos', component: BancoComponent },
  { path: 'cuentasBancarias', component: CuentaBancariaComponent },
  { path: 'grupoParametros', component: GrupoParametroComponent },
  { path: 'items', component: ItemComponent },
  { path: 'parametros', component: ParametroComponent },
  { path: 'periodos', component: PeriodoComponent },
  { path: 'series', component: SerieComponent },
  { path: 'socios', component: SocioComponent },
  { path: 'usuarios', component: UsuarioComponent },
  { path: 'historialPagos', component: HistorialPagoComponent },
  { path: 'notificaciones', component: NotificacionComponent },
  { path: 'pagos', component: PagoComponent },
  { path: 'tomaLecturas', component: RegistroConsumoComponent },
  { path: 'aprobacionLecturas', component: AprobacionLecturaComponent },
  { path: 'aprobacionPagos', component: AprobacionPagoComponent },
  { path: 'catastro', component: CatastroComponent }

];

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    LoginComponent,
    InicioComponent,
    RolComponent,
    RolAccesoComponent,
    AccesoComponent,
    SubMenuComponent,
    RegistroConsumoComponent,
    PagoComponent,
    HistorialPagoComponent,
    NotificacionComponent,
    UsuarioComponent,
    AsignacionMedidorComponent,
    BancoComponent,
    CuentaBancariaComponent,
    GrupoParametroComponent,
    ItemComponent,
    ParametroComponent,
    PeriodoComponent,
    SerieComponent,
    SocioComponent,
    AprobacionLecturaComponent,
    AprobacionPagoComponent,
    UsuarioRolComponent,
    CambioClaveComponent,
    CatastroComponent
  ],
  imports: [
    BrowserModule,
    //AppRoutingModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    BrowserAnimationsModule,
    MatIconModule,
    MatMenuModule,
    MatToolbarModule,
    MatButtonModule,
    CalendarModule,
    PanelModule,
    TableModule,
    FieldsetModule,
    DropdownModule,
    InputNumberModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatInputModule,
    FlexLayoutModule,
    MatListModule,
    MatSidenavModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
