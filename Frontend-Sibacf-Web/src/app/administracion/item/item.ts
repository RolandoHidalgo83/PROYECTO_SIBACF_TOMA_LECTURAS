export class Item {

    id_item: bigint;
    empresa_id: bigint;
    codigo: string;
    codigo_alterno: string;
    nombre: string;
    detalle: string;
    nemonico: string;
    indicador_serie: boolean;
    indicador_comercial: boolean;
    afecta_inventario: boolean;
    ultimo_precio_compra: number;
    fecha_ultimo_precio_compra: string;
    iva_id: bigint;
    ice_id: bigint;
    marca_id: bigint;
    tipo_inventario_id: bigint;
    tipo_producto_id: bigint;
    medida_d_id: bigint;
    medida_a_id: bigint;
    factor: number;
    producto_compuesto: boolean;
    maneja_lote: boolean;
    desagregar_produto_compuesto: boolean;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;

}
