export class Parametro {

    id_parametro: bigint;
    grupo_parametro_id: bigint;
    empresa_id: bigint;
    modulo_id: bigint;
    nombre: string;
    descripcion: string;
    nemonico: string;
    codigo_auxiliar: string;
    valor: string;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;
    
}
