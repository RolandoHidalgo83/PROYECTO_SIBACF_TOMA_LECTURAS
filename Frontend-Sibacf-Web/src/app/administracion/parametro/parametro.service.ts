import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import {Parametro } from './parametro';


@Injectable({
  providedIn: 'root'
})
export class ParametroService {
  private endPointPorNemonicoGrupo: string = '/parametro/getParametroGrupoParametroNeminico';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { 
    
  }
  /**
   * Permite listar por medió del nemónico del grupo
   * @param nemonicoGrupo
   */
  listarPorNemonicoGrupo(nemonicoGrupo: string): Observable<Parametro[]>
  {
    return this.http.get<Parametro[]>(`${this.utilitarioService.getURL()}${this.endPointPorNemonicoGrupo}/${nemonicoGrupo}`);
  }
}
