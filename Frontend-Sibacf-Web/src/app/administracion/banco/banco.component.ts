import { Component, OnInit } from '@angular/core';
import { SesionService } from '../../servicio/sesion.service';
import { Router } from '@angular/router';
import { Banco } from './banco';
import { BancoService } from './banco.service';
import Swal from 'sweetalert2';
import { Catalogo } from '../catalogo/catalogo';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-banco',
  templateUrl: './banco.component.html',
  styleUrls: ['./banco.component.css'],
  providers: [DatePipe]
})
export class BancoComponent implements OnInit {
  lista: Banco[];
  cabecera: Banco = new Banco();
  ubicacion: string = 'Gestión de Bancos';
  modoEdicion: boolean;
  registro: Banco = new Banco();
  activo: boolean;
  constructor(
    private sesionService: SesionService,
    private router: Router,
    private bancoService: BancoService,
    private utilitarioService: UtilitarioService,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    if (this.sesionService.getUsuarioId()) {
      this.obtenerListaRegistros();
      this.modoEdicion = false;
    } else {
      this.router.navigate(['./login']);
    }

  }
  /**
   * Permite obtener toda la lista de registros
   */
  obtenerListaRegistros(): void {
    this.bancoService.obtenerTodo()
      .subscribe(resp => {
        this.lista = resp;
      });
  }
  /**
   * Carga el registro para realizar la modificación
   */
  editar(r: Banco): void {
    this.modoEdicion = true;
    this.registro = r;
    let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
    let itm = lstEstados.filter(itm => itm.id_catalogo == r.estado_id)[0];

    if (itm.nemonico == 'ACT') {

      this.activo = true;
    } else {
      this.activo = false;
    }

  }
  /**
   * Almacena la información especificada
   */
  grabar(): void {

    this.registro.fecha_actualizacion = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
    this.registro.usuario_actualizacion = Number(this.sesionService.getUsuarioId());
    if (!this.registro.id_banco) {
      this.registro.id_banco = null;
      this.registro.fecha_creacion = this.datePipe.transform(new Date(), 'yyyy-MM-dd HH:mm:ss');
      this.registro.usuario_creacion = Number(this.sesionService.getUsuarioId());
    }
    let lstEstados: Catalogo[] = JSON.parse(this.sesionService.getEstados());
    let itm = lstEstados.filter(itm => itm.nemonico == 'ACT')[0];
    if (!this.activo) {
      itm = lstEstados.filter(itm => itm.nemonico == 'PAS')[0];
    }
    this.registro.estado_id = itm.id_catalogo;
    this.bancoService.guardar(this.registro)
      .subscribe(resp => {
        this.modoEdicion = false;
        if (!this.registro.id_banco) {
          this.lista.push(resp);
        }
        this.registro = new Banco();
      }, error => {
        Swal.fire('', 'Problemas al guardar la información', 'error');
      });

  }
  /**
   * Prepara para el ingreso de nueva información
   */
  nuevo(): void {
    this.modoEdicion = true;
    this.registro = new Banco();
    this.activo = true;
  }
  /**
   * Cancela el modo edición y regresa a la lista de registros
   */
  cancelar(): void {
    this.modoEdicion = false;
    this.registro = new Banco();
  }
  /**
   * Elimina un registro
   */
  eliminar(r: Banco): void {

    Swal.fire({
      text: "¿Esta seguro de que desea eliminar?",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Si, eliminar!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.bancoService.eliminar(r.id_banco)
          .subscribe(resp => {
            this.lista.splice(this.lista.indexOf(r), 1);
          }, error => {
            Swal.fire('', 'No se pudo eliminar el registro', error);
          });
      }
    });


  }
}
