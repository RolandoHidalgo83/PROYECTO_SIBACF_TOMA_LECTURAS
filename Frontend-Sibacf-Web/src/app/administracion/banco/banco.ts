export class Banco {
    id_banco: number;
    nombre: string;
    nemonico: string;
    codigo_auxiliar: string;
    valor_auxiliar: string;
    fecha_creacion: string;
    usuario_creacion: number;
    fecha_actualizacion: string;
    usuario_actualizacion: number;
    estado_id: number;
    columnas = [
        { campo: 'nombre', etiqueta: 'Banco' },
        { campo: 'nemonico', etiqueta: 'Código' },
        { campo: 'estado_id', etiqueta: 'Estado' },
    ];
}
