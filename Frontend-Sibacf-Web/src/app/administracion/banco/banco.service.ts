import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import { Banco } from './banco';

@Injectable({
  providedIn: 'root'
})
export class BancoService {
  private endPointLista: string = '/banco/getBanco';
  private endPointGrabar: string = '/banco/saveBanco';
  private endPointEliminar: string = '/banco/deleteBanco';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * Permite consumir el sw que lista todos los registros
   */
  obtenerTodo(): Observable<Banco[]> {
    return this.http.get<Banco[]>(`${this.utilitarioService.getURL()}${this.endPointLista}`);
  }
  /**
   * Permite consumir el sw que guarda la información
   * @param objeto
   */
  guardar(objeto: Object): Observable<Banco> {
    return this.http.post<Banco>(`${this.utilitarioService.getURL()}${this.endPointGrabar}`, objeto);
  }
  /**
   * Permite eliminar un registro por medio del id
   * @param id
   */
  eliminar(id: number): Observable<any>{
    console.log(`${this.utilitarioService.getURL()}${this.endPointEliminar}/${id}`);
    return this.http.get<any>(`${this.utilitarioService.getURL()}${this.endPointEliminar}/${id}`);
  }
}
