import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import { Periodo } from './periodo';

@Injectable({
  providedIn: 'root'
})
export class PeriodoService {
  private endPointListaAnios: string = '/periodo/getAnios';
  private endPointListaPorAnioPorEstado: string = '/periodo/getPeriodoAnio';
  private endPointObtenerPorId: string = '/periodo/getPeriodoId';
  private endPointendPointGrabar: string = '/periodo/deletePeriodo';

  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * Permite obtener los años disponibles
   */
  listarAnio():Observable<Periodo[]>{
return this.http.get<Periodo[]>(`${this.utilitarioService.getURL()}${this.endPointListaAnios}`);
  }
  listarPorAnio(anio: string, neomincoEstado: string): Observable<Periodo[]>{
    return this.http.get<Periodo[]>(`${this.utilitarioService.getURL()}${this.endPointListaPorAnioPorEstado}/${anio}/${neomincoEstado }`)
  }
}
