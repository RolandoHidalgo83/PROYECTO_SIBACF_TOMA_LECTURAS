export class Periodo {

    id_periodo: bigint;
    empresa_id: bigint;
    nombre: string;
    anio: number;
    mes: number;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;

}
