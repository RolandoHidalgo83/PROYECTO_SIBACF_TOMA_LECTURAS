import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AsignacionMedidor } from './asignacion-medidor';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AsignacionMedidorService {
  private endPointListarActivoPorFiltro = '/asignacionSerie/getAsignacionActivaPorFiltro';
  private endPointListarPorPersonaEmpresa = '/asignacionSerie/getAsignacionSeriePersonaEmpresa'
  private endPointGuardarUnRegistro = '/asignacionSerie/saveAsignacionSerie'
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }

  /**
 * Permite consumir el sw que lista todos los registros activos por filtro
 */
  obtenerActivoPorFiltro(filtro: string): Observable<AsignacionMedidor[]> {
    if (!filtro) {
      filtro = '@';
    }
    return this.http.get<AsignacionMedidor[]>(`${this.utilitarioService.getURL()}${this.endPointListarActivoPorFiltro}/${filtro}`);
  }
  /**
   * Permite guardar un registro con los campos especificado en el objeto
   * @param objeto 
   */
  guardarUnRegistro(objeto: Object): Observable<any>{
    return this.http.post(
      `${this.utilitarioService.getURL()}${this.endPointGuardarUnRegistro}`
      , objeto);
  }

  /**
* Permite consumir el sw que lista los medidores asignados a una persona de una empresa especifica
*/
  /* obtenerPorPersonaEmpresa(): Observable<AsignacionMedidor[]> {
    return this.http.get<AsignacionMedidor[]>(`${this.utilitarioService.getURL()}${this.endPointListar}`);
  } */

}
