export class AsignacionMedidor {
        id_asignacion_serie: number;
        serie_id: number;
        codigo_medidor: string;
        serie: string;
        persona_empresa_id: number;
        identificacion: string;
        razon_social: string
        barrio: string;
        fecha_instalacion: Date;
        lectura_inicial: number;
        ciclo: string;
        sector_id: number;
        ruta: string;
        manzana: string;
        numero_piso: number;
        numero_casa: string;
        numero_lote: number;
        longitud: number;
        latitud: number;
        orden: number;
        fecha_creacion: Date;
        usuario_creacion: number;
        fecha_actualizacion: Date;
        usuario_actualizacion: number;
        estado_id: number;
}
