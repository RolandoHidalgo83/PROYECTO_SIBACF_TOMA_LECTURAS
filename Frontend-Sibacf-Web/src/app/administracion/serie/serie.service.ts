import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SerieService {
  private endPointGrabar: string = '/serie/saveSerie';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }

  /**
   * Permite guardar un registro con los campos especificado en el objeto
   * @param objeto 
   */
  guardarUnRegistro(objeto: Object): Observable<any>{
    return this.http.post(
      `${this.utilitarioService.getURL()}${this.endPointGrabar}`
      , objeto);
  }
}
