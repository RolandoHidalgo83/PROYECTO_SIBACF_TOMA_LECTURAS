export class Serie {

    id_serie: bigint;
    articulo_id: bigint;
    codigo: string;
    serie: string;
    tipo: string;
    modelo: string;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;

}
