export class Catalogo {
    id_catalogo: number
    grupo_catalogo_id: number
    nombre: string;
    descripcion: string;
    nemonico: string;
    codigo_auxiliar: string;
    valor_auxiliar: string;
    fecha_creacion: Date;
    usuario_creacion: number;
    fecha_actualizacion: Date;
    usuario_actualizacion: number;
    estado_id: number;
}
