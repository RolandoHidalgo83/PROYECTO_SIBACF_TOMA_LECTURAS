import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import { Catalogo} from './catalogo';

@Injectable({
  providedIn: 'root'
})
export class CatalogoService {
private endPointPorNemonicoGrupoCatalogo='/catalogo/getCatalogoGrupoCatalogoNemonico';
private endPointPorNemonicoCatalogoNemonicoGrupo='/catalogo/getCatalogoNemonicoGrupoCatalogoNemonico';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * Permite consumir el sw que lista los registros por el nemónico del grupo
   */
  obtenerPorNemonicoGrupo(nemonicoGrupo: string): Observable<Catalogo[]> {
    return this.http.get<Catalogo[]>(`${this.utilitarioService.getURL()}${this.endPointPorNemonicoGrupoCatalogo}/${nemonicoGrupo}`);
  }
/**
 * Permite obtener por medio del nemónico del grupo y el nemónico del catálogo
 * @param nemonicoGrupo 
 * @param nemonicoCatalogo 
 */
  obtenerPorNemonicoGrupoPorNemonicoCatalogo(nemonicoGrupo: string, nemonicoCatalogo: string): Observable<Catalogo> {
    return this.http.get<Catalogo>(`${this.utilitarioService.getURL()}${this.endPointPorNemonicoCatalogoNemonicoGrupo}/${nemonicoGrupo}/${nemonicoCatalogo}`);
  }
}
