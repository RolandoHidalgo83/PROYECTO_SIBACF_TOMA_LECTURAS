export class CuentaBancaria {
        id_cuenta_bancaria: number;
        banco_id: number;
        tipo_cuenta_id: number;
        cuenta: string;
        nemonico: string;
        fecha_creacion: string;
        usuario_creacion: number;
        fecha_actualizacion: string;
        usuario_actualizacion: number;
        estado_id: number;
}
