import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import { CuentaBancaria} from './cuenta-bancaria';

@Injectable({
  providedIn: 'root'
})
export class CuentaBancariaService {
  private endPointPorBancoIdPorNemonicoEstado: string = '/cuentaBancaria/getCuentaBancariaPorBanco';

  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService
  ) { }
  /**
   * Permite consumir el sw que lista todos los registros
   */
  obtenerPorBancoId(bancoId: string, nemonicoEstado: string): Observable<CuentaBancaria[]> {
    return this.http.get<CuentaBancaria[]>(`${this.utilitarioService.getURL()}${this.endPointPorBancoIdPorNemonicoEstado}/${bancoId}/${nemonicoEstado}`);
  }
}
