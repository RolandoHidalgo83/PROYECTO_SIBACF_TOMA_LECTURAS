export class Socio {
 
    id_persona: bigint;
    tipo_identificacion_id: bigint;
    identificacion: string;
    personeria_id: bigint;
    nombres: string;
    apellidos: string;
    razon_social: string;
    nombre_comercial: string;
    genero_id: bigint;
    fecha_nacimiento: string;
    estado_civil_id: bigint;
    tipo_sangre_id: bigint;
    trato_id: bigint;
    codigo: string;
    es_relacionado: boolean;
    telefono_fijo: string;
    extension_fijo: string;
    telefono_alterno_fijo: string;
    extension_alterno_fijo: string;
    telefono_movil: string;
    correo_electronico: string;
    cc_correo_electronico: string;
    pagina_web: string;
    tipo_dicapacidad_id: bigint;
    porcentaje_discapacidad: number;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;
    persona_empresa_id: number;
}
