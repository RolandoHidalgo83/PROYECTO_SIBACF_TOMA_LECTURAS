import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { UtilitarioService } from '../../servicio/utilitario.service';
import { Observable } from 'rxjs';
import { Socio } from './socio';
import { SesionService } from '../../servicio/sesion.service';

@Injectable({
  providedIn: 'root'
})
export class SocioService {
  private endPointListarPorEmpresaIdPorCodigoGrupoPorFiltro: string = '/persona/getPersonaPorEmpresaIdPorCodigoGrupoPorFiltro';
  constructor(
    private http: HttpClient,
    private utilitarioService: UtilitarioService,
    private sesionService: SesionService
  ) { }

  /**
   * Permite listar por medio del id de la empresa, el código del grupo de persona y un filtro
   * @param codigoGrupo 
   * @param filtro 
   */
  listarPorFiltro(codigoGrupo: string, filtro: string):Observable<Socio[]>
  {
    if(filtro == undefined)
    {
      filtro = '@'
    }else
    {
      if(filtro.trim() == ''){
        filtro = '@'
      }
    }
    
    return this.http.get<Socio[]>(`${this.utilitarioService.getURL()}${this.endPointListarPorEmpresaIdPorCodigoGrupoPorFiltro}/${this.sesionService.getEmpresaId()}/${codigoGrupo}/${filtro}`);
  }
}
