export class GrupoParametro {

    id_grupo_parametro: bigint;
    nombre: string;
    descripcion: string;
    nemonico: string;
    codigo_auxiliar: string;
    fecha_creacion: string;
    usuario_creacion: bigint;
    fecha_actualizacion: string;
    usuario_actualizacion: bigint;
    estado_id: bigint;

}
