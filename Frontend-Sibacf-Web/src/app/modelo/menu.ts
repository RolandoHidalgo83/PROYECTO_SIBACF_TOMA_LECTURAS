export class Menu {
    id_acceso: bigint;
    acceso_padre_id: bigint;
    texto: string;
    accion: string;
    nivel: number;
    orden: number;
    icono: string;
    menuHijo: Menu[];
}
