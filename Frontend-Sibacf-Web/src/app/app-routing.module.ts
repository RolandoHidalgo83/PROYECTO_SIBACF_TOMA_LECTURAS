import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './principal/login/login.component';
import { InicioComponent } from './principal/inicio/inicio.component';
import { RegistroConsumoComponent } from './negocio/registro-consumo/registro-consumo.component';
import { PagoComponent } from './negocio/pago/pago.component';
import { HistorialPagoComponent } from './negocio/historial-pago/historial-pago.component';
import { NotificacionComponent } from './negocio/notificacion/notificacion.component';
import { UsuarioComponent } from './seguridades/usuario/usuario.component';
import { AsignacionMedidorComponent } from './administracion/asignacion-medidor/asignacion-medidor.component';
import { BancoComponent } from './administracion/banco/banco.component';
import { CuentaBancariaComponent } from './administracion/cuenta-bancaria/cuenta-bancaria.component';
import { GrupoParametroComponent } from './administracion/grupo-parametro/grupo-parametro.component';
import { ItemComponent } from './administracion/item/item.component';
import { ParametroComponent } from './administracion/parametro/parametro.component';
import { PeriodoComponent } from './administracion/periodo/periodo.component';
import { SerieComponent } from './administracion/serie/serie.component';
import { SocioComponent } from './administracion/socio/socio.component';
import { AprobacionLecturaComponent } from './negocio/aprobacion-lectura/aprobacion-lectura.component';
import { AprobacionPagoComponent } from './negocio/aprobacion-pago/aprobacion-pago.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: 'login', component: LoginComponent },
  { path: 'inicio', component: InicioComponent },
  
  { path: 'asignacionMedidores', component: AsignacionMedidorComponent },
  { path: 'bancos', component: BancoComponent },
  { path: 'cuentasBancarias', component: CuentaBancariaComponent },
  { path: 'grupoParametros', component: GrupoParametroComponent },
  { path: 'items', component: ItemComponent },
  { path: 'parametros', component: ParametroComponent },
  { path: 'periodos', component: PeriodoComponent },
  { path: 'series', component: SerieComponent },
  { path: 'socios', component: SocioComponent },
  { path: 'usuarios', component: UsuarioComponent },
  { path: 'historialPagos', component: HistorialPagoComponent },
  { path: 'notificaciones', component: NotificacionComponent },
  { path: 'pagos', component: PagoComponent },
  { path: 'tomaLecturas', component: RegistroConsumoComponent },
  { path: 'aprobacionLecturas', component: AprobacionLecturaComponent },
  { path: 'aprobacionPagos', component: AprobacionPagoComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
