import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';



@Injectable({
  providedIn: 'root'
  
})
export class UtilitarioService {
  
  //private URLSW: string = 'http://192.168.86.21:90/backend_sibacf_web/public/api';
  //private URLSW: string = 'http://localhost/backend_sibacf_web/public/api';
  //private URLSW: string = 'http://www.jaapcolaisa.com/backend_sibacf_web/public/api';
  private URLSW: string = 'http://127.0.0.1:8000/api';
  
  constructor(
  ) { }

  /**
   * Obtiene la URL principal para la invocacion de los servicios web
   */
  getURL(): string{
    return this.URLSW;
  }

  /**
   * Recibe como parametro de entrada una cadena para devolver la misma cadena encriptada en MD5
   * @param dato 
   */
  getDatoEncriptadoEnMD5(dato: string): string
  { 
    return CryptoJS.MD5(dato).toString();
  }

}
