import { Injectable } from '@angular/core';
import { Catalogo } from '../administracion/catalogo/catalogo';
import { UsuarioAutenticado } from '../principal/login/usuario-autenticado';

const SESSION_TOKEN = 'Token';
const SESSION_USUARIO = 'Usuario';
const SESSION_ESTADOS = 'Estados';

@Injectable({
  providedIn: 'root'
})
export class SesionService {

  constructor() { }

  /**
   * Ingresa el token en sesion.
  */
  public setToken(token: string): void {
    window.sessionStorage.removeItem(SESSION_TOKEN);
    window.sessionStorage.setItem(SESSION_TOKEN, token);
  }

  /**
   * Obtiene el token de session.
  */
  public getToken(): string {
    return window.sessionStorage.getItem(SESSION_TOKEN);
  }

  /**
  * Obtiene el id de la empresa de session.
 */
  public getEmpresaId(): string {
    let usuarioAutenticado: UsuarioAutenticado = JSON.parse(this.getUsuario());
    if (usuarioAutenticado) {
      if (usuarioAutenticado.id_empresa) {
        return usuarioAutenticado.id_empresa.toString();
      } else {
        return '';
      }
    } else {
      return '';
    }
  }
  /**
   * Obtiene el id del usuario de session.
  */
  public getUsuarioId(): string {
    let usuarioAutenticado: UsuarioAutenticado = JSON.parse(this.getUsuario());
    if (usuarioAutenticado) {
      return usuarioAutenticado.id_usuario.toString();
    } else {
      return '';
    }

  }
  /**
   * Obtiene el login de session.
  */
  public getLogin(): string {
    let usuarioAutenticado: UsuarioAutenticado = JSON.parse(this.getUsuario());
    if (usuarioAutenticado) {
      return usuarioAutenticado.usuario;
    } else {
      return '';
    }
  }
  /**
   * Obtiene el tipo de usuario de session
   */
  public getTipoUsuarioId(): string {
    let usuarioAutenticado: UsuarioAutenticado = JSON.parse(this.getUsuario());
    if (usuarioAutenticado) {
      if (usuarioAutenticado.tipo_usuario_id) {
        return usuarioAutenticado.tipo_usuario_id.toString();
      } else {
        return '';
      }
    } else {
      return '';
    }

  }
  /**
   * Obtiene el código del tipo de usuario de session
   */
  public getNemonicoTipoUsuario(): string{
    let usuarioAutenticado: UsuarioAutenticado = JSON.parse(this.getUsuario());
    if (usuarioAutenticado) {
      return usuarioAutenticado.nemonico_tipo_usuario;
    } else {
      return '';
    }
  }
  /**
   * Obtiene el id de la persona empresa de session
   */
  public getPersonaEmpresaId(): string {
    let usuarioAutenticado: UsuarioAutenticado = JSON.parse(this.getUsuario());
    if (usuarioAutenticado) {
      if (usuarioAutenticado.id_persona_empresa) {
        return usuarioAutenticado.id_persona_empresa.toString();
      } else {
        return '';
      }
    } else {
      return '';
    }

  }
  /**
     * Ingresa el registro del usuario autenticado en sesion.
    */
  public setUsuario(usuarioAutenticado: UsuarioAutenticado): void {
    window.sessionStorage.removeItem(SESSION_USUARIO);
    window.sessionStorage.setItem(SESSION_USUARIO, JSON.stringify(usuarioAutenticado));
  }

  /**
   * Obtiene el usuario autenticado de session.
  */
  public getUsuario(): string {
    return window.sessionStorage.getItem(SESSION_USUARIO);
  }

  /**
   * Ingresa los estados en sesion.
  */
  public setEstados(catalogos: Catalogo[]): void {
    window.sessionStorage.removeItem(SESSION_ESTADOS);
    window.sessionStorage.setItem(SESSION_ESTADOS, JSON.stringify(catalogos));
  }

  /**
   * Obtiene el id del usuario de session.
  */
  public getEstados(): string {
    return window.sessionStorage.getItem(SESSION_ESTADOS);
  }
  /**
   * Remueve todas las sesiones existentes
   */
  public cerrarSesion() {
    window.sessionStorage.clear();
  }

}
